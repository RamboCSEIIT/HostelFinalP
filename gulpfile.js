global.printMsg = false;
var gulp = require('gulp');
global.browserSync = require('browser-sync').create();
//var runSequence = require('gulp-run-sequence');
//var runSequence                           = require('run-sequence');
//var gulpSequence = require('gulp-sequence').use(gulp);
var requireDir = require('require-dir');
var tasks = requireDir('./zz_tasks');
var gulpSequence = require('gulp-sequence');


 


 
var SASS_CHECK_PATH = '00_SASS/**/*.*';
var SCRIPTS_IN_CHECK_PATH = '01_SCRIPTS/**/*.*';
var IMAGES_CHECK_PATH = 'public/02_IMAGES/**/*.*';
var TEST_PATH = 'BootingFiles/**/*.*';

var VIEW_PATH_BLADE_T = 'ViewsT/**/*.*';
var PHP_PATH = 'ServerPart/src/**/*.php';
var VIEW_PATH_BLADE_T_OUT = 'ServerPart/Views';

//Creation and deletion of directories
//////////////////////////////////////////////////////////////////////
gulp.task('clean-without-image', tasks.aaa_common_init.clean_without_image);
gulp.task('clean-image', tasks.aaa_common_init.clean_image); 
//////////////////////////////////////////////////////////////////////
//Image compression
//////////////////////////////////////////////////////////////////////
gulp.task('images', tasks.ac_image);
////////////////////////////////////////////////////////////////////// 
//////////////////////////////////////////////////////////////////////
///Environment variables
gulp.task('clear', tasks.aaa_common_init.clear);
gulp.task('test_env', tasks.aaa_common_init.test_env);
gulp.task('test_env_dev', tasks.aaa_common_init.test_env_dev);
gulp.task('test_env_prod', tasks.aaa_common_init.test_env_prod);
gulp.task('set_dev_node_env', tasks.aaa_common_init.set_dev_node_env);
gulp.task('set_prod_node_env', tasks.aaa_common_init.set_prod_node_env);
gulp.task('browser_init', tasks.aaa_common_init.browser_init);
gulp.task('browser_reload', tasks.aaa_common_init.browser_reload);

gulp.task('js-error', tasks.ab_java_script.jsError);
gulp.task('js_move_home', tasks.ab_java_script.js_move_home);

 
 
 


gulp.task('sass_move_home', tasks.af_sass.sass_move_home);

gulp.task('browser_reload', tasks.aaa_common_init.browser_reload);
//////////////////////////////////////////////////////////////////////
//////HTML 5
//////////////////////////////////////////////////////////////////////
gulp.task('minify-html5', tasks.ae_html.html5_minify);
gulp.task('home-body', tasks.ae_html.html5_move_home_body);
//////////////////////////////////////////////////////////////

 gulp.task('image-seq', function (cb)
{

    gulpSequence('clean-image','images','browser_reload')(cb);

    
});

 

gulp.task('sass-seq', function (cb)
{

    gulpSequence('sass_move_home','browser_reload')(cb);

    
});






 

gulp.task('js-seq', function (cb)
{

    gulpSequence( 'js-error', 'js_move_home','browser_reload')(cb);

        
});




gulp.task('MoveView', function (cb)
{
  
    gulpSequence('home-body','minify-html5','browser_reload')(cb);

  
   
    return;
});

 






gulp.task('default', function ( )
{
    gulp.watch(PHP_PATH, function (  )
    {

        console.log('Reload Controller Php Started   -----------------------------------------------------');
        global.browserSync.reload();


    });
/*
    gulp.watch(VIEW_PATH_BLADE_T, function ()
    {
        console.log(' VIEW 1   -----------------------------------------------------');
        gulpSequence( 'MoveView')();
        global.browserSync.reload();
        console.log(' VIEW 2   -----------------------------------------------------');

    });
*/
    gulp.watch(TEST_PATH, function ()
    {

        console.log('Reload Font Started   -----------------------------------------------------');
        global.browserSync.reload();


    });

   

    
    gulp.watch(VIEW_PATH_BLADE_T, ['MoveView']);
    gulp.watch(SASS_CHECK_PATH, ['sass-seq']);

    gulp.watch(SCRIPTS_IN_CHECK_PATH, ['js-seq']);







    return;
});

gulp.task('develop', function (cb) {
    gulpSequence(
            'clean-without-image',
            'set_dev_node_env',
            'MoveView',
            'test_env',
            'js-seq',
            'sass-seq',
            'browser_init',
            'default',
            cb);
});

gulp.task('deploy', function (cb) {
    gulpSequence(
            'clean-without-image',
            'set_prod_node_env',
            'MoveView',
            'test_env',
            'js-seq',
            'sass-seq',
            'browser_init',
            'default',
            cb);
});




