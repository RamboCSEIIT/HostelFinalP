var gulp                                                = require('gulp');
var del                                                  = require('del');
 
 
 










var clear                                        = require('clear-screen');
//use this for deletion
var CSS_OUT_PATH_D                                = 'ServerPart/public/00_CSS/**/*.*';
var SCRIPTS_OUT_PATH_D                        = 'ServerPart/public/01_SCRIPTS/**/*.*';
var VIEW_PATH_BLADE_T                                    = 'ServerPart/Views/**/*.*';
var IMAGES_CHECK_PATH                          = 'ServerPart/public/02_IMAGES/**/*.*';
 
module.exports = 
{
  clear: function () 
  {
       clear();
  },
  test_env: function () 
  {
      if(global.printMsg)
       console.log(process.env.NODE_ENV + ":: Test ENV---------------------------");
  }
  ,    
  test_env_dev: function () 
  {
    if(global.printMsg)
      console.log( process.env.NODE_ENV+"::development---------------------------");
  }
  ,
  test_env_prod: function () 
  {    
    if(global.printMsg)
      console.log(process.env.NODE_ENV + "::production---------------------------");
  }  
  ,
  set_dev_node_env : function () 
  {
    
    return process.env.NODE_ENV =  'development';
  }
  ,
  set_prod_node_env: function () 
  {   
   
    return process.env.NODE_ENV =  'production';
  }
  ,
  browser_init : function () 
  {        

        global.browserSync.init(
                            {
                                proxy: 
                                {
                                   target: "localhost", // can be [virtual host, sub-directory, localhost with port]
                                //    ws: true // enables websockets
                                    // target: "localhost/register"
                                }
                            }
                        );  
      
   },
  browser_reload : function () 
  {   
      
         global.browserSync.reload();
       
      
   },
   move_font: function () 
   {
     
     
      
       return gulp.src(FONT_IN_PATH)
		 .pipe(gulp.dest(FONT_OUT_PATH));
                
   
   
        

       
      
   },
   clean_without_image: function () 
   {
     
       
       var result= del.sync([
		CSS_OUT_PATH_D,SCRIPTS_OUT_PATH_D,VIEW_PATH_BLADE_T
	]);
        
      
        return result;
      
   },
   clean_image: function () 
   {
     
       
       var result= del.sync([
		IMAGES_CHECK_PATH
	]);
        
      
        return result;
      
   }
   
  
};