var gulp = require('gulp');


var  htmlmin = require('gulp-htmlmin');
var gulpif = require('gulp-if');

 
var VIEW_PATH_BLADE_T                                         = 'ViewsT/**/*.*';
var VIEW_PATH_BLADE_T_OUT                                     = 'ServerPart/Views';

var VIEW_PATH_BLADE_T_HOME           = 'ViewsT/ab_Fragments/aa_HomePage/**/*.*';
var VIEW_PATH_BLADE_T_OUT_HOME   = 'ViewsT/aa_WorkSpace/aa_homepage/aa_include/';
var concat = require('gulp-concat');

module.exports =
{
            html5_minify: function ()
            {
              //  console.log('VIEW 2a-----------------------------------------------------');      
                         
               return   gulp.src(VIEW_PATH_BLADE_T)
               .pipe(gulpif(process.env.NODE_ENV === 'production',htmlmin({
                    collapseWhitespace: true,
                    removeAttributeQuotes: true,
                    removeComments: true,
                    minifyJS: true,
                    //ignoreCustomFragments: [/@for[\s\S]*@endfor/, /@if[\s\S]*@endif/, /@php[\s\S]*@endphp/,/"{!!.*!!}"/,]
                }) ))

                .pipe(gulp.dest(VIEW_PATH_BLADE_T_OUT));

            },



            html5_move_home_body: function ()
            {

                //console.log('VIEW 2b-----------------------------------------------------');     

                return gulp.src(VIEW_PATH_BLADE_T_HOME)
                .pipe(concat('body.blade.php'))
                .pipe(gulp.dest(VIEW_PATH_BLADE_T_OUT_HOME));

            }

 };
 
 
 
  

