var gulp = require('gulp');
var jshint = require('gulp-jshint');
var notify = require('gulp-notify');
// process.env.NODE_ENV
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');

var prettify = require('gulp-jsbeautifier');
var removeEmptyLines = require('gulp-remove-empty-lines');


var SCRIPTS_IN_PATH = '01_SCRIPTS/**/*.*';
var SCRIPTS_OUT_PATH = 'ServerPart/public/01_SCRIPTS';

var SCRIPTS_IN_PATH_HOME = '01_SCRIPTS/aa_Home_page/**/*.*';
module.exports =
        {

            jsError: function ()
            {

                var result =
                        gulp.src(SCRIPTS_IN_PATH)
                        .pipe(jshint())
                        // Use gulp-notify as jshint reporter
                        .pipe(notify(function (file)
                        {
                            if (file.jshint.success)
                            {
                                // Don't show something if success
                                return false;
                            }

                            var errors = file.jshint.results.map(function (data)
                            {
                                if (data.error)
                                {
                                    return "(" + data.error.line + ':' + data.error.character + ') ' + data.error.reason;
                                }
                            }).join("\n");

                            return file.relative + " (" + file.jshint.results.length + " errors)\n" + errors;
                        }));
                //  console.log('JS Error Ended Started -----------------------------------------------------');
                return result;

            },
            js_move_home: function ()
            {
                // test
                // process.env.NODE_ENV =  'development';
                // process.env.NODE_ENV =  'production';
                var result =
                        gulp.src(SCRIPTS_IN_PATH_HOME)
                        .pipe(gulpif(process.env.NODE_ENV === 'development', sourcemaps.init()))
                        .pipe(concat('script_home.js'))
                        .pipe(removeEmptyLines())


                        .pipe(gulpif(process.env.NODE_ENV === 'development',
                                prettify({
                                    mode: 'VERIFY_AND_WRITE'
                                }),
                                uglify()

                                ))
                        .pipe(gulpif(process.env.NODE_ENV === 'development', sourcemaps.write()))
                        .pipe(sourcemaps.write())
                        .pipe(gulp.dest(SCRIPTS_OUT_PATH))
                        .pipe(notify({message: 'home page Javascript task complete'}));
                return result;




            }


        };