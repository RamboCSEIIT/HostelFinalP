
var gulp = require('gulp');
 
 
var sass = require('gulp-sass');
var gulpif = require('gulp-if');
var SASS_IN_PATH_STYLE_HOME = '00_SASS/aa_HOME_PAGE/**/*.*';
var SASS_OUT_PATH_STYLE_HOME = 'ServerPart/public/00_CSS';
var autoprefixer = require('gulp-autoprefixer');
var notify = require('gulp-notify');

var cssbeautify = require('gulp-cssbeautify');
var concat = require('gulp-concat');



module.exports =
        {

            sass_move_home: function ()
            {
                return gulp.src(SASS_IN_PATH_STYLE_HOME)
                
                
                      
                        .pipe(gulpif(process.env.NODE_ENV === 'development',
                                sass().on('error', function (err)
                        {
                            notify().write(err);
                            this.emit('end');
                        }),
                                sass(
                                        {
                                            outputStyle: 'compressed',
                                        }
                                ).on('error', function (err)
                        {
                            notify().write(err);
                            this.emit('end');
                        })))
                        .pipe(gulpif(process.env.NODE_ENV === 'development', cssbeautify({
                            indent: '  ',
                            openbrace: 'separate-line',
                            autosemicolon: true
                        })))
                         .pipe(autoprefixer())       
                        .pipe(concat('aa_home_page.css'))
                        .pipe(gulp.dest(SASS_OUT_PATH_STYLE_HOME));

            }



        };