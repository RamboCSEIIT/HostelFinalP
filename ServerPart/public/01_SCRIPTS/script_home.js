  $('.owl-carousel').owlCarousel({
      // CSS Styles
      baseClass: "owl-carousel",
      theme: "owl-theme",
      loop: true,
      autoplay: true,
      margin: 10,
      responsiveClass: true,
      nav: true,
      rewind: true,
      responsive: {
          0: {
              items: 1,
              nav: true
          },
          600: {
              items: 3,
              nav: false
          },
          1000: {
              items: 3,
              nav: true,
              loop: false
          }
      }
  });
  $(window).scroll(function() {
      $(".slideanim").each(function() {
          var pos = $(this).offset().top;
          var winTop = $(window).scrollTop();
          if (pos < winTop + 600) {
              $(this).addClass("slide");
          }
      });
  });
  $(document).ready(function() {
      // Add smooth scrolling to all links in navbar + footer link
      $(".navbar a,#myFooter a[href='#package'],#myFooter a[href='#contact'],#myFooter a[href='#topnavid'], #myFooter a[href='#wayanad'], #myFooter a[href='#profile'] ").on('click', function(event) {
          // Make sure this.hash has a value before overriding default behavior
          if (this.hash !== "") {
              // Prevent default anchor click behavior
              event.preventDefault();
              // Store hash
              var hash = this.hash;
              // Using jQuery's animate() method to add smooth page scroll
              // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
              $('html, body').animate({
                  scrollTop: $(hash).offset().top
              }, 900, function() {
                  // Add hash (#) to URL when done scrolling (default click behavior)
                  window.location.hash = hash;
              });
          } // End if
      });
  });
  $(document).ready(function() {
      function myMap() {
          var myCenter = new google.maps.LatLng(21.6700, 835.9578);
          var mapProp = {
              center: myCenter,
              zoom: 12,
              scrollwheel: false,
              draggable: false,
              mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          var map = new google.maps.Map(document.getElementById("map"), mapProp);
          var marker = new google.maps.Marker({
              position: myCenter
          });
          marker.setMap(map);
      }
      google.maps.event.addDomListener(window, 'load', initMap, {
          passive: true
      });
  });

  function initMap() {
      // Create a new StyledMapType object, passing it an array of styles,
      // and the name to be displayed on the map type control.
      var styledMapType = new google.maps.StyledMapType(
          [{
                  elementType: 'geometry',
                  stylers: [{
                      color: '#ebe3cd'
                  }]
              },
              {
                  elementType: 'labels.text.fill',
                  stylers: [{
                      color: '#523735'
                  }]
              },
              {
                  elementType: 'labels.text.stroke',
                  stylers: [{
                      color: '#f5f1e6'
                  }]
              },
              {
                  featureType: 'administrative',
                  elementType: 'geometry.stroke',
                  stylers: [{
                      color: '#c9b2a6'
                  }]
              },
              {
                  featureType: 'administrative.land_parcel',
                  elementType: 'geometry.stroke',
                  stylers: [{
                      color: '#dcd2be'
                  }]
              },
              {
                  featureType: 'administrative.land_parcel',
                  elementType: 'labels.text.fill',
                  stylers: [{
                      color: '#ae9e90'
                  }]
              },
              {
                  featureType: 'landscape.natural',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#dfd2ae'
                  }]
              },
              {
                  featureType: 'poi',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#dfd2ae'
                  }]
              },
              {
                  featureType: 'poi',
                  elementType: 'labels.text.fill',
                  stylers: [{
                      color: '#93817c'
                  }]
              },
              {
                  featureType: 'poi.park',
                  elementType: 'geometry.fill',
                  stylers: [{
                      color: '#a5b076'
                  }]
              },
              {
                  featureType: 'poi.park',
                  elementType: 'labels.text.fill',
                  stylers: [{
                      color: '#447530'
                  }]
              },
              {
                  featureType: 'road',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#f5f1e6'
                  }]
              },
              {
                  featureType: 'road.arterial',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#fdfcf8'
                  }]
              },
              {
                  featureType: 'road.highway',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#f8c967'
                  }]
              },
              {
                  featureType: 'road.highway',
                  elementType: 'geometry.stroke',
                  stylers: [{
                      color: '#e9bc62'
                  }]
              },
              {
                  featureType: 'road.highway.controlled_access',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#e98d58'
                  }]
              },
              {
                  featureType: 'road.highway.controlled_access',
                  elementType: 'geometry.stroke',
                  stylers: [{
                      color: '#db8555'
                  }]
              },
              {
                  featureType: 'road.local',
                  elementType: 'labels.text.fill',
                  stylers: [{
                      color: '#806b63'
                  }]
              },
              {
                  featureType: 'transit.line',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#dfd2ae'
                  }]
              },
              {
                  featureType: 'transit.line',
                  elementType: 'labels.text.fill',
                  stylers: [{
                      color: '#8f7d77'
                  }]
              },
              {
                  featureType: 'transit.line',
                  elementType: 'labels.text.stroke',
                  stylers: [{
                      color: '#ebe3cd'
                  }]
              },
              {
                  featureType: 'transit.station',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#dfd2ae'
                  }]
              },
              {
                  featureType: 'water',
                  elementType: 'geometry.fill',
                  stylers: [{
                      color: '#b9d3c2'
                  }]
              },
              {
                  featureType: 'water',
                  elementType: 'labels.text.fill',
                  stylers: [{
                      color: '#92998d'
                  }]
              }
          ], {
              name: 'Styled Map'
          });
      // Create a map object, and include the MapTypeId to add
      // to the map type control.
      var map = new google.maps.Map(document.getElementById('map'), {
          center: {
              lat: 11.6700,
              lng: 75.9578
          },
          zoom: 11,
          mapTypeControlOptions: {
              mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain',
                  'styled_map'
              ]
          }
      });
      //Associate the styled map with the MapTypeId and set it to display.
      map.mapTypes.set('styled_map', styledMapType);
      map.setMapTypeId('styled_map');
      var myCenter = new google.maps.LatLng(11.6700, 75.9578);
      var marker = new google.maps.Marker({
          position: myCenter
      });
      marker.setMap(map);
  }
  $('.navbar-nav>li>a').on('click', function() {
      var id = $(this).attr('id');
      // alert(id);
      if (id != "admin_navdrop")
          $('.navbar-collapse').collapse('hide');
  });
  $('.navbar-collapse').on('shown.bs.collapse', function() {
      //alert("hello");
      $('#tag').html("Menu");
      $('#tag').removeClass("fa fa-bars");
      $('#tag').addClass("fa fa-times");
  });
  $('.navbar-collapse').on('hidden.bs.collapse', function() {
      // $('#tag').html("O");
      $('#tag').html("Menu");
      $('#tag').removeClass("fa fa-times");
      $('#tag').addClass("fa fa-bars");
  });
  // magic.js
  $(document).ready(function() {
      // process the form
      $('#formsub').submit(function(event) {
          // get the form data
          // there are many ways to get this data using jQuery (you can use the class or id also)
          var formData = {
              //   'name': $('input[name=name]').val(),
              'email': $('input[name=email]').val(),
              //  'superheroAlias': $('input[name=superheroAlias]').val()
          };
          // process the form
          $.ajax({
                  type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                  url: '/subs', // the url where we want to POST
                  data: formData, // our data object
                  dataType: 'json', // what type of data do we expect back from the server
                  encode: true
              })
              // using the done promise callback
              .done(function(data) {
                  // log data to the console so we can see
                  console.log(data);
                  // here we will handle errors and validation messages
                  if (!data.success) {
                      $("#submsg").html(data.errors.email);
                      /*                        // handle errors for name ---------------
                                              if (data.errors.name) {
                                                  $('#name-group').addClass('has-error'); // add the error class to show red input
                                                  $('#name-group').append('<div class="help-block">' + data.errors.name + '</div>'); // add the actual error message under our input
                                              }
                                              // handle errors for email ---------------
                                              if (data.errors.email) {
                                                  $('#email-group').addClass('has-error'); // add the error class to show red input
                                                  $('#email-group').append('<div class="help-block">' + data.errors.email + '</div>'); // add the actual error message under our input
                                              }
                                              // handle errors for superhero alias ---------------
                                              if (data.errors.superheroAlias) {
                                                  $('#superhero-group').addClass('has-error'); // add the error class to show red input
                                                  $('#superhero-group').append('<div class="help-block">' + data.errors.superheroAlias + '</div>'); // add the actual error message under our input
                                              }*/
                  } else {
                      // ALL GOOD! just show the success message!
                      // $('form').append('<div class="alert alert-success">' + data.message + '</div>');
                      // usually after form submission, you'll want to redirect
                      // window.location = '/thank-you'; // redirect a user to another page
                      //  alert('success'+data.message); // for now we'll just alert the user
                      $("#submsg").html(data.message);
                  }
              });
          // stop the form from submitting the normal way and refreshing the page
          event.preventDefault();
      });
  });
  // magic.js
  $(document).ready(function() {
      // process the form
      $('#contactId').submit(function(event) {
          // get the form data
          // there are many ways to get this data using jQuery (you can use the class or id also)
          var formData = {
              'ame': $('input[name=name]').val(),
              'email': $('input[name=email]').val(),
              'comments': $('input[name=comments]').val()
          };
          // process the form
          $.ajax({
                  type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                  url: '/contact', // the url where we want to POST
                  data: formData, // our data object
                  dataType: 'json', // what type of data do we expect back from the server
                  encode: true
              })
              // using the done promise callback
              .done(function(data) {
                  // log data to the console so we can see
                  console.log(data);
                  // here we will handle errors and validation messages
                  if (!data.success) {
                      $("#contactmsg").html(data.errors.email);
                      /*                        // handle errors for name ---------------
                       if (data.errors.name) {
                       $('#name-group').addClass('has-error'); // add the error class to show red input
                       $('#name-group').append('<div class="help-block">' + data.errors.name + '</div>'); // add the actual error message under our input
                       }
                       // handle errors for email ---------------
                       if (data.errors.email) {
                       $('#email-group').addClass('has-error'); // add the error class to show red input
                       $('#email-group').append('<div class="help-block">' + data.errors.email + '</div>'); // add the actual error message under our input
                       }
                       // handle errors for superhero alias ---------------
                       if (data.errors.superheroAlias) {
                       $('#superhero-group').addClass('has-error'); // add the error class to show red input
                       $('#superhero-group').append('<div class="help-block">' + data.errors.superheroAlias + '</div>'); // add the actual error message under our input
                       }*/
                  } else {
                      // ALL GOOD! just show the success message!
                      // $('form').append('<div class="alert alert-success">' + data.message + '</div>');
                      // usually after form submission, you'll want to redirect
                      // window.location = '/thank-you'; // redirect a user to another page
                      //  alert('success'+data.message); // for now we'll just alert the user
                      $("#contactmsg").html(data.message);
                  }
              });
          // stop the form from submitting the normal way and refreshing the page
          event.preventDefault();
      });
      $('#enquiryform').submit(function(event) {
          //  var result = $('#enqid').val();
          var name = $('#name').val();
          var email = $('#email').val();
          var mobile = $('#mobile').val();
          var comments = $('#comments').val();
          // alert("hai");
          // get the form data
          // there are many ways to get this data using jQuery (you can use the class or id also)
          var formData = {
              'name': name,
              'email': email,
              'comments': comments,
              'mobile': mobile
          };
          // console.log("Hello :: "+formData);
          // 
          // 
          // process the form
          // alert(formData.name+"::"+formData.email +"::"+formData.mobile+"::"+formData.comments);
          $.ajax({
                  type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                  url: '/contact', // the url where we want to POST
                  data: formData, // our data object
                  dataType: 'json', // what type of data do we expect back from the server
                  encode: true
              })
              // using the done promise callback
              .done(function(data) {
                  // log data to the console so we can see
                  console.log(data);
                  // here we will handle errors and validation messages
                  if (!data.success) {
                      $("#contactmsg").html(data.message);
                      /*                        // handle errors for name ---------------
                       if (data.errors.name) {
                       $('#name-group').addClass('has-error'); // add the error class to show red input
                       $('#name-group').append('<div class="help-block">' + data.errors.name + '</div>'); // add the actual error message under our input
                       }
                       // handle errors for email ---------------
                       if (data.errors.email) {
                       $('#email-group').addClass('has-error'); // add the error class to show red input
                       $('#email-group').append('<div class="help-block">' + data.errors.email + '</div>'); // add the actual error message under our input
                       }
                       // handle errors for superhero alias ---------------
                       if (data.errors.superheroAlias) {
                       $('#superhero-group').addClass('has-error'); // add the error class to show red input
                       $('#superhero-group').append('<div class="help-block">' + data.errors.superheroAlias + '</div>'); // add the actual error message under our input
                       }*/
                  } else {
                      // ALL GOOD! just show the success message!
                      // $('form').append('<div class="alert alert-success">' + data.message + '</div>');
                      // usually after form submission, you'll want to redirect
                      // window.location = '/thank-you'; // redirect a user to another page
                      //  alert('success'+data.message); // for now we'll just alert the user
                      $("#contactmsg").html(data.message);
                  }
              });
          // stop the form from submitting the normal way and refreshing the page
          event.preventDefault();
      });
  });
  // A $( document ).ready() block.
  $(document).ready(function() {
      $("#wayanad_1").addClass("active");
  });
  $(document).ready(function() {
      for (var i = 0; i < 9; i++) {
          if (i % 3 == 0)
              $("#package_" + i).addClass("cardSet0");
          if (i % 3 == 1)
              $("#package_" + i).addClass("cardSet1");
          if (i % 3 == 2)
              $("#package_" + i).addClass("cardSet2");
      }
  });
  $(document).ready(function() {
      $(".card-text").addClass("text-justify");
  });
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFhX293bF9jYXJvdXNlbC9vd2xfY2Fyb3VzZWwuanMiLCJhYl9TbGlkZV9Gcm9tX0JvdHRvbS9zbGlkZV9ib3R0b20uanMiLCJhY19tb3ZlX2FuaW0vbW92ZV9hbmltLmpzIiwiYWRfZ29vZ2xlX21hcC9nb29nbGUtbWFwLmpzIiwiYWVfbW9iaWxlX21lbnUvbWVudV9tb2JpbGUuanMiLCJhZl9zdWJzY3JpcHRpb24vc3Vic2NyaXB0aW9uLmpzIiwiYWdfY29udGFjdC9jb250YWN0LmpzIiwiYWhfd2F5YW5hZC93YXlhbmFkLmpzIiwiYWlfcGFja2FnZS9wYWNrYWdlLmpzIiwiYWlfdGV4dGJhbGFuY2UvYmFsYW5jZS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUM3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDWEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDaENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDcktBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUN6QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3RFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNqS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ0pBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDbEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoic2NyaXB0X2hvbWUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgIFxuICAkKCcub3dsLWNhcm91c2VsJykub3dsQ2Fyb3VzZWwoe1xuICBcbiAgICAgLy8gQ1NTIFN0eWxlc1xuICAgIGJhc2VDbGFzcyA6IFwib3dsLWNhcm91c2VsXCIsXG4gICAgdGhlbWUgOiBcIm93bC10aGVtZVwiLFxuICAgIGxvb3A6dHJ1ZSxcbiAgICBhdXRvcGxheTp0cnVlLCAgICAgICAgXG4gICAgbWFyZ2luOjEwLFxuICAgIHJlc3BvbnNpdmVDbGFzczp0cnVlLFxuICAgIG5hdjp0cnVlLFxuICAgIHJld2luZDp0cnVlLFxuICAgIHJlc3BvbnNpdmU6e1xuICAgICAgICAwOntcbiAgICAgICAgICAgIGl0ZW1zOjEsXG4gICAgICAgICAgICBuYXY6dHJ1ZVxuICAgICAgICB9LFxuICAgICAgICA2MDA6e1xuICAgICAgICAgICAgaXRlbXM6MyxcbiAgICAgICAgICAgIG5hdjpmYWxzZVxuICAgICAgICB9LFxuICAgICAgICAxMDAwOntcbiAgICAgICAgICAgIGl0ZW1zOjMsXG4gICAgICAgICAgICBuYXY6dHJ1ZSxcbiAgICAgICAgICAgIGxvb3A6ZmFsc2VcbiAgICAgICAgfVxuICAgIH1cbn0pO1xuXG4iLCIgICQod2luZG93KS5zY3JvbGwoZnVuY3Rpb24oKSB7XG4gICQoXCIuc2xpZGVhbmltXCIpLmVhY2goZnVuY3Rpb24oKXtcbiAgICB2YXIgcG9zID0gJCh0aGlzKS5vZmZzZXQoKS50b3A7XG5cbiAgICB2YXIgd2luVG9wID0gJCh3aW5kb3cpLnNjcm9sbFRvcCgpO1xuICAgIGlmIChwb3MgPCB3aW5Ub3AgKyA2MDApIHtcbiAgICAgICQodGhpcykuYWRkQ2xhc3MoXCJzbGlkZVwiKTtcbiAgICB9XG4gIH0pO1xufSk7ICAgICAgICAgXG4gXG4iLCIkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKVxue1xuICAgIC8vIEFkZCBzbW9vdGggc2Nyb2xsaW5nIHRvIGFsbCBsaW5rcyBpbiBuYXZiYXIgKyBmb290ZXIgbGlua1xuICAgICQoXCIubmF2YmFyIGEsI215Rm9vdGVyIGFbaHJlZj0nI3BhY2thZ2UnXSwjbXlGb290ZXIgYVtocmVmPScjY29udGFjdCddLCNteUZvb3RlciBhW2hyZWY9JyN0b3BuYXZpZCddLCAjbXlGb290ZXIgYVtocmVmPScjd2F5YW5hZCddLCAjbXlGb290ZXIgYVtocmVmPScjcHJvZmlsZSddIFwiKS5vbignY2xpY2snLCBmdW5jdGlvbiAoZXZlbnQpIHtcblxuICAgICAgICAvLyBNYWtlIHN1cmUgdGhpcy5oYXNoIGhhcyBhIHZhbHVlIGJlZm9yZSBvdmVycmlkaW5nIGRlZmF1bHQgYmVoYXZpb3JcbiAgICAgICAgaWYgKHRoaXMuaGFzaCAhPT0gXCJcIilcbiAgICAgICAge1xuXG4gICAgICAgICAgICAvLyBQcmV2ZW50IGRlZmF1bHQgYW5jaG9yIGNsaWNrIGJlaGF2aW9yXG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgICAvLyBTdG9yZSBoYXNoXG4gICAgICAgICAgICB2YXIgaGFzaCA9IHRoaXMuaGFzaDtcblxuICAgICAgICAgICAgLy8gVXNpbmcgalF1ZXJ5J3MgYW5pbWF0ZSgpIG1ldGhvZCB0byBhZGQgc21vb3RoIHBhZ2Ugc2Nyb2xsXG4gICAgICAgICAgICAvLyBUaGUgb3B0aW9uYWwgbnVtYmVyICg5MDApIHNwZWNpZmllcyB0aGUgbnVtYmVyIG9mIG1pbGxpc2Vjb25kcyBpdCB0YWtlcyB0byBzY3JvbGwgdG8gdGhlIHNwZWNpZmllZCBhcmVhXG4gICAgICAgICAgICAkKCdodG1sLCBib2R5JykuYW5pbWF0ZSh7XG4gICAgICAgICAgICAgICAgc2Nyb2xsVG9wOiAkKGhhc2gpLm9mZnNldCgpLnRvcFxuICAgICAgICAgICAgfSwgOTAwLCBmdW5jdGlvbiAoKSB7XG5cbiAgICAgICAgICAgICAgICAvLyBBZGQgaGFzaCAoIykgdG8gVVJMIHdoZW4gZG9uZSBzY3JvbGxpbmcgKGRlZmF1bHQgY2xpY2sgYmVoYXZpb3IpXG4gICAgICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhhc2ggPSBoYXNoO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0gLy8gRW5kIGlmXG4gICAgfSk7XG5cblxuXG5cblxuXG59KTsiLCJcbiBcblxuXG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpXG57XG5cblxuICAgIGZ1bmN0aW9uIG15TWFwKCkge1xuXG4gICAgICAgIHZhciBteUNlbnRlciA9IG5ldyBnb29nbGUubWFwcy5MYXRMbmcoMjEuNjcwMCwgODM1Ljk1NzgpO1xuICAgICAgICB2YXIgbWFwUHJvcCA9IHtjZW50ZXI6IG15Q2VudGVyLCB6b29tOiAxMiwgc2Nyb2xsd2hlZWw6IGZhbHNlLCBkcmFnZ2FibGU6IGZhbHNlLCBtYXBUeXBlSWQ6IGdvb2dsZS5tYXBzLk1hcFR5cGVJZC5ST0FETUFQfTtcbiAgICAgICAgdmFyIG1hcCA9IG5ldyBnb29nbGUubWFwcy5NYXAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJtYXBcIiksIG1hcFByb3ApO1xuICAgICAgICB2YXIgbWFya2VyID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7cG9zaXRpb246IG15Q2VudGVyfSk7XG4gICAgICAgIG1hcmtlci5zZXRNYXAobWFwKTtcbiAgICB9XG5cbiAgICBnb29nbGUubWFwcy5ldmVudC5hZGREb21MaXN0ZW5lcih3aW5kb3csICdsb2FkJywgaW5pdE1hcCwge3Bhc3NpdmU6IHRydWV9KTtcblxuXG5cblxufSk7XG5cbiBcbmZ1bmN0aW9uIGluaXRNYXAoKSB7XG5cbiAgICAvLyBDcmVhdGUgYSBuZXcgU3R5bGVkTWFwVHlwZSBvYmplY3QsIHBhc3NpbmcgaXQgYW4gYXJyYXkgb2Ygc3R5bGVzLFxuICAgIC8vIGFuZCB0aGUgbmFtZSB0byBiZSBkaXNwbGF5ZWQgb24gdGhlIG1hcCB0eXBlIGNvbnRyb2wuXG4gICAgdmFyIHN0eWxlZE1hcFR5cGUgPSBuZXcgZ29vZ2xlLm1hcHMuU3R5bGVkTWFwVHlwZShcbiAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICB7ZWxlbWVudFR5cGU6ICdnZW9tZXRyeScsIHN0eWxlcnM6IFt7Y29sb3I6ICcjZWJlM2NkJ31dfSxcbiAgICAgICAgICAgICAgICB7ZWxlbWVudFR5cGU6ICdsYWJlbHMudGV4dC5maWxsJywgc3R5bGVyczogW3tjb2xvcjogJyM1MjM3MzUnfV19LFxuICAgICAgICAgICAgICAgIHtlbGVtZW50VHlwZTogJ2xhYmVscy50ZXh0LnN0cm9rZScsIHN0eWxlcnM6IFt7Y29sb3I6ICcjZjVmMWU2J31dfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAnYWRtaW5pc3RyYXRpdmUnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5LnN0cm9rZScsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjYzliMmE2J31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAnYWRtaW5pc3RyYXRpdmUubGFuZF9wYXJjZWwnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5LnN0cm9rZScsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjZGNkMmJlJ31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAnYWRtaW5pc3RyYXRpdmUubGFuZF9wYXJjZWwnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2xhYmVscy50ZXh0LmZpbGwnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnI2FlOWU5MCd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ2xhbmRzY2FwZS5uYXR1cmFsJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeScsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjZGZkMmFlJ31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAncG9pJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeScsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjZGZkMmFlJ31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAncG9pJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdsYWJlbHMudGV4dC5maWxsJyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyM5MzgxN2MnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdwb2kucGFyaycsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnZ2VvbWV0cnkuZmlsbCcsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjYTViMDc2J31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAncG9pLnBhcmsnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2xhYmVscy50ZXh0LmZpbGwnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnIzQ0NzUzMCd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3JvYWQnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5JyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNmNWYxZTYnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdyb2FkLmFydGVyaWFsJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeScsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjZmRmY2Y4J31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAncm9hZC5oaWdod2F5JyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeScsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjZjhjOTY3J31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAncm9hZC5oaWdod2F5JyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeS5zdHJva2UnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnI2U5YmM2Mid9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3JvYWQuaGlnaHdheS5jb250cm9sbGVkX2FjY2VzcycsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnZ2VvbWV0cnknLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnI2U5OGQ1OCd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3JvYWQuaGlnaHdheS5jb250cm9sbGVkX2FjY2VzcycsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnZ2VvbWV0cnkuc3Ryb2tlJyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNkYjg1NTUnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdyb2FkLmxvY2FsJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdsYWJlbHMudGV4dC5maWxsJyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyM4MDZiNjMnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICd0cmFuc2l0LmxpbmUnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5JyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNkZmQyYWUnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICd0cmFuc2l0LmxpbmUnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2xhYmVscy50ZXh0LmZpbGwnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnIzhmN2Q3Nyd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3RyYW5zaXQubGluZScsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnbGFiZWxzLnRleHQuc3Ryb2tlJyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNlYmUzY2QnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICd0cmFuc2l0LnN0YXRpb24nLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5JyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNkZmQyYWUnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICd3YXRlcicsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnZ2VvbWV0cnkuZmlsbCcsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjYjlkM2MyJ31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAnd2F0ZXInLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2xhYmVscy50ZXh0LmZpbGwnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnIzkyOTk4ZCd9XVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICB7bmFtZTogJ1N0eWxlZCBNYXAnfSk7XG5cbiAgICAvLyBDcmVhdGUgYSBtYXAgb2JqZWN0LCBhbmQgaW5jbHVkZSB0aGUgTWFwVHlwZUlkIHRvIGFkZFxuICAgIC8vIHRvIHRoZSBtYXAgdHlwZSBjb250cm9sLlxuICAgIHZhciBtYXAgPSBuZXcgZ29vZ2xlLm1hcHMuTWFwKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdtYXAnKSwge1xuICAgICAgICBjZW50ZXI6IHtsYXQ6IDExLjY3MDAsIGxuZzogNzUuOTU3OH0sXG4gICAgICAgIHpvb206IDExLFxuICAgICAgICBtYXBUeXBlQ29udHJvbE9wdGlvbnM6IHtcbiAgICAgICAgICAgIG1hcFR5cGVJZHM6IFsncm9hZG1hcCcsICdzYXRlbGxpdGUnLCAnaHlicmlkJywgJ3RlcnJhaW4nLFxuICAgICAgICAgICAgICAgICdzdHlsZWRfbWFwJ11cbiAgICAgICAgfVxuICAgIH0pO1xuXG4gICAgLy9Bc3NvY2lhdGUgdGhlIHN0eWxlZCBtYXAgd2l0aCB0aGUgTWFwVHlwZUlkIGFuZCBzZXQgaXQgdG8gZGlzcGxheS5cbiAgICBtYXAubWFwVHlwZXMuc2V0KCdzdHlsZWRfbWFwJywgc3R5bGVkTWFwVHlwZSk7XG4gICAgbWFwLnNldE1hcFR5cGVJZCgnc3R5bGVkX21hcCcpO1xuXG4gICAgdmFyIG15Q2VudGVyID0gbmV3IGdvb2dsZS5tYXBzLkxhdExuZygxMS42NzAwLCA3NS45NTc4KTtcbiAgICB2YXIgbWFya2VyID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7cG9zaXRpb246IG15Q2VudGVyfSk7XG4gICAgbWFya2VyLnNldE1hcChtYXApO1xuICAgIFxuICAgICBcblxuICAgIFxufSIsIiAgICAkKCcubmF2YmFyLW5hdj5saT5hJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKVxuICAgIHtcbiAgICAgICAgICAgdmFyIGlkID0gJCh0aGlzKS5hdHRyKCdpZCcpO1xuICAgICAgICAgICAvLyBhbGVydChpZCk7XG4gICAgICAgICAgIGlmKGlkICE9IFwiYWRtaW5fbmF2ZHJvcFwiKVxuICAgICAgICAgICAgICAgICAgJCgnLm5hdmJhci1jb2xsYXBzZScpLmNvbGxhcHNlKCdoaWRlJyk7XG4gICAgfSk7XG4gICAgXG4gICAgJCgnLm5hdmJhci1jb2xsYXBzZScpLm9uKCdzaG93bi5icy5jb2xsYXBzZScsIGZ1bmN0aW9uKClcbiAgICB7XG4gICAgICAgLy9hbGVydChcImhlbGxvXCIpO1xuICAgICAgICAkKCcjdGFnJykuaHRtbChcIk1lbnVcIik7XG4gICAgICAgICQoJyN0YWcnKS5yZW1vdmVDbGFzcyhcImZhIGZhLWJhcnNcIik7XG4gICAgICAgICQoJyN0YWcnKS5hZGRDbGFzcyhcImZhIGZhLXRpbWVzXCIpO1xuICAgICAgICBcbiAgICAgICBcbiAgICB9KTtcbiAgICBcbiAgICBcbiAgICAkKCcubmF2YmFyLWNvbGxhcHNlJykub24oJ2hpZGRlbi5icy5jb2xsYXBzZScsIGZ1bmN0aW9uKClcbiAgICB7XG4gICAgICAvLyAkKCcjdGFnJykuaHRtbChcIk9cIik7XG4gICAgICAgICAgICAgJCgnI3RhZycpLmh0bWwoXCJNZW51XCIpO1xuICAgICAgICAgICAgJCgnI3RhZycpLnJlbW92ZUNsYXNzKFwiZmEgZmEtdGltZXNcIik7XG4gICAgICAgICAgICAgICQoJyN0YWcnKS5hZGRDbGFzcyhcImZhIGZhLWJhcnNcIik7XG4gICAgfSk7IiwiLy8gbWFnaWMuanNcbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcblxuICAgIC8vIHByb2Nlc3MgdGhlIGZvcm1cbiAgICAkKCcjZm9ybXN1YicpLnN1Ym1pdChmdW5jdGlvbiAoZXZlbnQpIHtcblxuICAgICAgICAvLyBnZXQgdGhlIGZvcm0gZGF0YVxuICAgICAgICAvLyB0aGVyZSBhcmUgbWFueSB3YXlzIHRvIGdldCB0aGlzIGRhdGEgdXNpbmcgalF1ZXJ5ICh5b3UgY2FuIHVzZSB0aGUgY2xhc3Mgb3IgaWQgYWxzbylcbiAgICAgICAgdmFyIGZvcm1EYXRhID0ge1xuICAgICAgICAgLy8gICAnbmFtZSc6ICQoJ2lucHV0W25hbWU9bmFtZV0nKS52YWwoKSxcbiAgICAgICAgICAgICdlbWFpbCc6ICQoJ2lucHV0W25hbWU9ZW1haWxdJykudmFsKCksXG4gICAgICAgICAgLy8gICdzdXBlcmhlcm9BbGlhcyc6ICQoJ2lucHV0W25hbWU9c3VwZXJoZXJvQWxpYXNdJykudmFsKClcbiAgICAgICAgfTtcblxuICAgICAgICAvLyBwcm9jZXNzIHRoZSBmb3JtXG4gICAgICAgICQuYWpheCh7XG4gICAgICAgICAgICB0eXBlOiAnUE9TVCcsIC8vIGRlZmluZSB0aGUgdHlwZSBvZiBIVFRQIHZlcmIgd2Ugd2FudCB0byB1c2UgKFBPU1QgZm9yIG91ciBmb3JtKVxuICAgICAgICAgICAgdXJsOiAnL3N1YnMnLCAvLyB0aGUgdXJsIHdoZXJlIHdlIHdhbnQgdG8gUE9TVFxuICAgICAgICAgICAgZGF0YTogZm9ybURhdGEsIC8vIG91ciBkYXRhIG9iamVjdFxuICAgICAgICAgICAgZGF0YVR5cGU6ICdqc29uJywgLy8gd2hhdCB0eXBlIG9mIGRhdGEgZG8gd2UgZXhwZWN0IGJhY2sgZnJvbSB0aGUgc2VydmVyXG4gICAgICAgICAgICBlbmNvZGU6IHRydWVcbiAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAvLyB1c2luZyB0aGUgZG9uZSBwcm9taXNlIGNhbGxiYWNrXG4gICAgICAgICAgICAgICAgLmRvbmUoZnVuY3Rpb24gKGRhdGEpIHtcblxuICAgICAgICAgICAgICAgICAgICAvLyBsb2cgZGF0YSB0byB0aGUgY29uc29sZSBzbyB3ZSBjYW4gc2VlXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpO1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIGhlcmUgd2Ugd2lsbCBoYW5kbGUgZXJyb3JzIGFuZCB2YWxpZGF0aW9uIG1lc3NhZ2VzXG4gICAgICAgICAgICAgICAgICAgIGlmICghZGF0YS5zdWNjZXNzKSBcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjc3VibXNnXCIpLmh0bWwoZGF0YS5lcnJvcnMuZW1haWwpO1xuLyogICAgICAgICAgICAgICAgICAgICAgICAvLyBoYW5kbGUgZXJyb3JzIGZvciBuYW1lIC0tLS0tLS0tLS0tLS0tLVxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEuZXJyb3JzLm5hbWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjbmFtZS1ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTsgLy8gYWRkIHRoZSBlcnJvciBjbGFzcyB0byBzaG93IHJlZCBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNuYW1lLWdyb3VwJykuYXBwZW5kKCc8ZGl2IGNsYXNzPVwiaGVscC1ibG9ja1wiPicgKyBkYXRhLmVycm9ycy5uYW1lICsgJzwvZGl2PicpOyAvLyBhZGQgdGhlIGFjdHVhbCBlcnJvciBtZXNzYWdlIHVuZGVyIG91ciBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBoYW5kbGUgZXJyb3JzIGZvciBlbWFpbCAtLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLmVycm9ycy5lbWFpbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNlbWFpbC1ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTsgLy8gYWRkIHRoZSBlcnJvciBjbGFzcyB0byBzaG93IHJlZCBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNlbWFpbC1ncm91cCcpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImhlbHAtYmxvY2tcIj4nICsgZGF0YS5lcnJvcnMuZW1haWwgKyAnPC9kaXY+Jyk7IC8vIGFkZCB0aGUgYWN0dWFsIGVycm9yIG1lc3NhZ2UgdW5kZXIgb3VyIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBlcnJvcnMgZm9yIHN1cGVyaGVybyBhbGlhcyAtLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLmVycm9ycy5zdXBlcmhlcm9BbGlhcykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNzdXBlcmhlcm8tZ3JvdXAnKS5hZGRDbGFzcygnaGFzLWVycm9yJyk7IC8vIGFkZCB0aGUgZXJyb3IgY2xhc3MgdG8gc2hvdyByZWQgaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjc3VwZXJoZXJvLWdyb3VwJykuYXBwZW5kKCc8ZGl2IGNsYXNzPVwiaGVscC1ibG9ja1wiPicgKyBkYXRhLmVycm9ycy5zdXBlcmhlcm9BbGlhcyArICc8L2Rpdj4nKTsgLy8gYWRkIHRoZSBhY3R1YWwgZXJyb3IgbWVzc2FnZSB1bmRlciBvdXIgaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgIH0qL1xuXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIEFMTCBHT09EISBqdXN0IHNob3cgdGhlIHN1Y2Nlc3MgbWVzc2FnZSFcbiAgICAgICAgICAgICAgICAgICAgICAgLy8gJCgnZm9ybScpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImFsZXJ0IGFsZXJ0LXN1Y2Nlc3NcIj4nICsgZGF0YS5tZXNzYWdlICsgJzwvZGl2PicpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB1c3VhbGx5IGFmdGVyIGZvcm0gc3VibWlzc2lvbiwgeW91J2xsIHdhbnQgdG8gcmVkaXJlY3RcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHdpbmRvdy5sb2NhdGlvbiA9ICcvdGhhbmsteW91JzsgLy8gcmVkaXJlY3QgYSB1c2VyIHRvIGFub3RoZXIgcGFnZVxuICAgICAgICAgICAgICAgICAgICAgIC8vICBhbGVydCgnc3VjY2VzcycrZGF0YS5tZXNzYWdlKTsgLy8gZm9yIG5vdyB3ZSdsbCBqdXN0IGFsZXJ0IHRoZSB1c2VyXG4gICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgJChcIiNzdWJtc2dcIikuaHRtbChkYXRhLm1lc3NhZ2UpO1xuXG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgIC8vIHN0b3AgdGhlIGZvcm0gZnJvbSBzdWJtaXR0aW5nIHRoZSBub3JtYWwgd2F5IGFuZCByZWZyZXNoaW5nIHRoZSBwYWdlXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgfSk7XG5cbn0pOyIsIi8vIG1hZ2ljLmpzXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XG5cbiAgICAvLyBwcm9jZXNzIHRoZSBmb3JtXG4gICAgJCgnI2NvbnRhY3RJZCcpLnN1Ym1pdChmdW5jdGlvbiAoZXZlbnQpIHtcblxuICAgICAgICAvLyBnZXQgdGhlIGZvcm0gZGF0YVxuICAgICAgICAvLyB0aGVyZSBhcmUgbWFueSB3YXlzIHRvIGdldCB0aGlzIGRhdGEgdXNpbmcgalF1ZXJ5ICh5b3UgY2FuIHVzZSB0aGUgY2xhc3Mgb3IgaWQgYWxzbylcbiAgICAgICAgdmFyIGZvcm1EYXRhID0ge1xuICAgICAgICAgICAgJ2FtZSc6ICQoJ2lucHV0W25hbWU9bmFtZV0nKS52YWwoKSxcbiAgICAgICAgICAgICdlbWFpbCc6ICQoJ2lucHV0W25hbWU9ZW1haWxdJykudmFsKCksXG4gICAgICAgICAgICAnY29tbWVudHMnOiAkKCdpbnB1dFtuYW1lPWNvbW1lbnRzXScpLnZhbCgpXG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gcHJvY2VzcyB0aGUgZm9ybVxuICAgICAgICAkLmFqYXgoe1xuICAgICAgICAgICAgdHlwZTogJ1BPU1QnLCAvLyBkZWZpbmUgdGhlIHR5cGUgb2YgSFRUUCB2ZXJiIHdlIHdhbnQgdG8gdXNlIChQT1NUIGZvciBvdXIgZm9ybSlcbiAgICAgICAgICAgIHVybDogJy9jb250YWN0JywgLy8gdGhlIHVybCB3aGVyZSB3ZSB3YW50IHRvIFBPU1RcbiAgICAgICAgICAgIGRhdGE6IGZvcm1EYXRhLCAvLyBvdXIgZGF0YSBvYmplY3RcbiAgICAgICAgICAgIGRhdGFUeXBlOiAnanNvbicsIC8vIHdoYXQgdHlwZSBvZiBkYXRhIGRvIHdlIGV4cGVjdCBiYWNrIGZyb20gdGhlIHNlcnZlclxuICAgICAgICAgICAgZW5jb2RlOiB0cnVlXG4gICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLy8gdXNpbmcgdGhlIGRvbmUgcHJvbWlzZSBjYWxsYmFja1xuICAgICAgICAgICAgICAgIC5kb25lKGZ1bmN0aW9uIChkYXRhKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gbG9nIGRhdGEgdG8gdGhlIGNvbnNvbGUgc28gd2UgY2FuIHNlZVxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcblxuICAgICAgICAgICAgICAgICAgICAvLyBoZXJlIHdlIHdpbGwgaGFuZGxlIGVycm9ycyBhbmQgdmFsaWRhdGlvbiBtZXNzYWdlc1xuICAgICAgICAgICAgICAgICAgICBpZiAoIWRhdGEuc3VjY2VzcylcbiAgICAgICAgICAgICAgICAgICAge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAkKFwiI2NvbnRhY3Rtc2dcIikuaHRtbChkYXRhLmVycm9ycy5lbWFpbCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAvKiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBlcnJvcnMgZm9yIG5hbWUgLS0tLS0tLS0tLS0tLS0tXG4gICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEuZXJyb3JzLm5hbWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjbmFtZS1ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTsgLy8gYWRkIHRoZSBlcnJvciBjbGFzcyB0byBzaG93IHJlZCBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNuYW1lLWdyb3VwJykuYXBwZW5kKCc8ZGl2IGNsYXNzPVwiaGVscC1ibG9ja1wiPicgKyBkYXRhLmVycm9ycy5uYW1lICsgJzwvZGl2PicpOyAvLyBhZGQgdGhlIGFjdHVhbCBlcnJvciBtZXNzYWdlIHVuZGVyIG91ciBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgICAvLyBoYW5kbGUgZXJyb3JzIGZvciBlbWFpbCAtLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5lcnJvcnMuZW1haWwpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjZW1haWwtZ3JvdXAnKS5hZGRDbGFzcygnaGFzLWVycm9yJyk7IC8vIGFkZCB0aGUgZXJyb3IgY2xhc3MgdG8gc2hvdyByZWQgaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjZW1haWwtZ3JvdXAnKS5hcHBlbmQoJzxkaXYgY2xhc3M9XCJoZWxwLWJsb2NrXCI+JyArIGRhdGEuZXJyb3JzLmVtYWlsICsgJzwvZGl2PicpOyAvLyBhZGQgdGhlIGFjdHVhbCBlcnJvciBtZXNzYWdlIHVuZGVyIG91ciBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgICAvLyBoYW5kbGUgZXJyb3JzIGZvciBzdXBlcmhlcm8gYWxpYXMgLS0tLS0tLS0tLS0tLS0tXG4gICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEuZXJyb3JzLnN1cGVyaGVyb0FsaWFzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgJCgnI3N1cGVyaGVyby1ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTsgLy8gYWRkIHRoZSBlcnJvciBjbGFzcyB0byBzaG93IHJlZCBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNzdXBlcmhlcm8tZ3JvdXAnKS5hcHBlbmQoJzxkaXYgY2xhc3M9XCJoZWxwLWJsb2NrXCI+JyArIGRhdGEuZXJyb3JzLnN1cGVyaGVyb0FsaWFzICsgJzwvZGl2PicpOyAvLyBhZGQgdGhlIGFjdHVhbCBlcnJvciBtZXNzYWdlIHVuZGVyIG91ciBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgIH0qL1xuXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIEFMTCBHT09EISBqdXN0IHNob3cgdGhlIHN1Y2Nlc3MgbWVzc2FnZSFcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vICQoJ2Zvcm0nKS5hcHBlbmQoJzxkaXYgY2xhc3M9XCJhbGVydCBhbGVydC1zdWNjZXNzXCI+JyArIGRhdGEubWVzc2FnZSArICc8L2Rpdj4nKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gdXN1YWxseSBhZnRlciBmb3JtIHN1Ym1pc3Npb24sIHlvdSdsbCB3YW50IHRvIHJlZGlyZWN0XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB3aW5kb3cubG9jYXRpb24gPSAnL3RoYW5rLXlvdSc7IC8vIHJlZGlyZWN0IGEgdXNlciB0byBhbm90aGVyIHBhZ2VcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vICBhbGVydCgnc3VjY2VzcycrZGF0YS5tZXNzYWdlKTsgLy8gZm9yIG5vdyB3ZSdsbCBqdXN0IGFsZXJ0IHRoZSB1c2VyXG5cbiAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjY29udGFjdG1zZ1wiKS5odG1sKGRhdGEubWVzc2FnZSk7XG5cbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgLy8gc3RvcCB0aGUgZm9ybSBmcm9tIHN1Ym1pdHRpbmcgdGhlIG5vcm1hbCB3YXkgYW5kIHJlZnJlc2hpbmcgdGhlIHBhZ2VcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICB9KTtcblxuXG5cbiAgICAkKCcjZW5xdWlyeWZvcm0nKS5zdWJtaXQoZnVuY3Rpb24gKGV2ZW50KSB7XG5cbiAgICAgLy8gIHZhciByZXN1bHQgPSAkKCcjZW5xaWQnKS52YWwoKTtcbiAgICAgICB2YXIgbmFtZSA9ICQoJyNuYW1lJykudmFsKCk7XG4gICAgICAgdmFyIGVtYWlsPSQoJyNlbWFpbCcpLnZhbCgpO1xuICAgICAgIHZhciBtb2JpbGU9JCgnI21vYmlsZScpLnZhbCgpO1xuICAgICAgIHZhciBjb21tZW50cz0kKCcjY29tbWVudHMnKS52YWwoKTtcbiAgICAgXG4gICAgICAgXG4gICAgICAgLy8gYWxlcnQoXCJoYWlcIik7XG5cbiAgICAgICAgLy8gZ2V0IHRoZSBmb3JtIGRhdGFcbiAgICAgICAgLy8gdGhlcmUgYXJlIG1hbnkgd2F5cyB0byBnZXQgdGhpcyBkYXRhIHVzaW5nIGpRdWVyeSAoeW91IGNhbiB1c2UgdGhlIGNsYXNzIG9yIGlkIGFsc28pXG4gICAgICAgIHZhciBmb3JtRGF0YSA9IHtcbiAgICAgICAgICAgICAnbmFtZSc6IG5hbWUsXG4gICAgICAgICAgICAgJ2VtYWlsJzogZW1haWwsXG4gICAgICAgICAgICdjb21tZW50cyc6IGNvbW1lbnRzLFxuICAgICAgICAgICAgICdtb2JpbGUnICA6bW9iaWxlXG4gICAgICAgIH07XG4gICAgICAgLy8gY29uc29sZS5sb2coXCJIZWxsbyA6OiBcIitmb3JtRGF0YSk7XG4gICAgICAgLy8gXG4gICAgICAgLy8gXG4gICAgICAgIC8vIHByb2Nlc3MgdGhlIGZvcm1cbiAgICAgICAgXG4gICAgICAgLy8gYWxlcnQoZm9ybURhdGEubmFtZStcIjo6XCIrZm9ybURhdGEuZW1haWwgK1wiOjpcIitmb3JtRGF0YS5tb2JpbGUrXCI6OlwiK2Zvcm1EYXRhLmNvbW1lbnRzKTtcbiAgICAgICAgJC5hamF4KHtcbiAgICAgICAgICAgIHR5cGU6ICdQT1NUJywgLy8gZGVmaW5lIHRoZSB0eXBlIG9mIEhUVFAgdmVyYiB3ZSB3YW50IHRvIHVzZSAoUE9TVCBmb3Igb3VyIGZvcm0pXG4gICAgICAgICAgICB1cmw6ICcvY29udGFjdCcsIC8vIHRoZSB1cmwgd2hlcmUgd2Ugd2FudCB0byBQT1NUXG4gICAgICAgICAgICBkYXRhOiBmb3JtRGF0YSwgLy8gb3VyIGRhdGEgb2JqZWN0XG4gICAgICAgICAgICBkYXRhVHlwZTogJ2pzb24nLCAvLyB3aGF0IHR5cGUgb2YgZGF0YSBkbyB3ZSBleHBlY3QgYmFjayBmcm9tIHRoZSBzZXJ2ZXJcbiAgICAgICAgICAgIGVuY29kZTogdHJ1ZVxuICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC8vIHVzaW5nIHRoZSBkb25lIHByb21pc2UgY2FsbGJhY2tcbiAgICAgICAgICAgICAgICAuZG9uZShmdW5jdGlvbiAoZGF0YSkge1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIGxvZyBkYXRhIHRvIHRoZSBjb25zb2xlIHNvIHdlIGNhbiBzZWVcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YSk7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gaGVyZSB3ZSB3aWxsIGhhbmRsZSBlcnJvcnMgYW5kIHZhbGlkYXRpb24gbWVzc2FnZXNcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFkYXRhLnN1Y2Nlc3MpXG4gICAgICAgICAgICAgICAgICAgIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNjb250YWN0bXNnXCIpLmh0bWwoZGF0YS5tZXNzYWdlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qICAgICAgICAgICAgICAgICAgICAgICAgLy8gaGFuZGxlIGVycm9ycyBmb3IgbmFtZSAtLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5lcnJvcnMubmFtZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNuYW1lLWdyb3VwJykuYWRkQ2xhc3MoJ2hhcy1lcnJvcicpOyAvLyBhZGQgdGhlIGVycm9yIGNsYXNzIHRvIHNob3cgcmVkIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgJCgnI25hbWUtZ3JvdXAnKS5hcHBlbmQoJzxkaXYgY2xhc3M9XCJoZWxwLWJsb2NrXCI+JyArIGRhdGEuZXJyb3JzLm5hbWUgKyAnPC9kaXY+Jyk7IC8vIGFkZCB0aGUgYWN0dWFsIGVycm9yIG1lc3NhZ2UgdW5kZXIgb3VyIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBlcnJvcnMgZm9yIGVtYWlsIC0tLS0tLS0tLS0tLS0tLVxuICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLmVycm9ycy5lbWFpbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNlbWFpbC1ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTsgLy8gYWRkIHRoZSBlcnJvciBjbGFzcyB0byBzaG93IHJlZCBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNlbWFpbC1ncm91cCcpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImhlbHAtYmxvY2tcIj4nICsgZGF0YS5lcnJvcnMuZW1haWwgKyAnPC9kaXY+Jyk7IC8vIGFkZCB0aGUgYWN0dWFsIGVycm9yIG1lc3NhZ2UgdW5kZXIgb3VyIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBlcnJvcnMgZm9yIHN1cGVyaGVybyBhbGlhcyAtLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5lcnJvcnMuc3VwZXJoZXJvQWxpYXMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjc3VwZXJoZXJvLWdyb3VwJykuYWRkQ2xhc3MoJ2hhcy1lcnJvcicpOyAvLyBhZGQgdGhlIGVycm9yIGNsYXNzIHRvIHNob3cgcmVkIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgJCgnI3N1cGVyaGVyby1ncm91cCcpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImhlbHAtYmxvY2tcIj4nICsgZGF0YS5lcnJvcnMuc3VwZXJoZXJvQWxpYXMgKyAnPC9kaXY+Jyk7IC8vIGFkZCB0aGUgYWN0dWFsIGVycm9yIG1lc3NhZ2UgdW5kZXIgb3VyIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgfSovXG5cbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gQUxMIEdPT0QhIGp1c3Qgc2hvdyB0aGUgc3VjY2VzcyBtZXNzYWdlIVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gJCgnZm9ybScpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImFsZXJ0IGFsZXJ0LXN1Y2Nlc3NcIj4nICsgZGF0YS5tZXNzYWdlICsgJzwvZGl2PicpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB1c3VhbGx5IGFmdGVyIGZvcm0gc3VibWlzc2lvbiwgeW91J2xsIHdhbnQgdG8gcmVkaXJlY3RcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHdpbmRvdy5sb2NhdGlvbiA9ICcvdGhhbmsteW91JzsgLy8gcmVkaXJlY3QgYSB1c2VyIHRvIGFub3RoZXIgcGFnZVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gIGFsZXJ0KCdzdWNjZXNzJytkYXRhLm1lc3NhZ2UpOyAvLyBmb3Igbm93IHdlJ2xsIGp1c3QgYWxlcnQgdGhlIHVzZXJcblxuICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNjb250YWN0bXNnXCIpLmh0bWwoZGF0YS5tZXNzYWdlKTtcblxuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAvLyBzdG9wIHRoZSBmb3JtIGZyb20gc3VibWl0dGluZyB0aGUgbm9ybWFsIHdheSBhbmQgcmVmcmVzaGluZyB0aGUgcGFnZVxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIH0pO1xuXG5cblxufSk7XG5cblxuXG5cblxuXG4iLCIvLyBBICQoIGRvY3VtZW50ICkucmVhZHkoKSBibG9jay5cbiQoIGRvY3VtZW50ICkucmVhZHkoZnVuY3Rpb24oKSB7XG4gICAgJChcIiN3YXlhbmFkXzFcIikuYWRkQ2xhc3MoXCJhY3RpdmVcIik7XG5cbn0pOyIsIiQoIGRvY3VtZW50ICkucmVhZHkoZnVuY3Rpb24oKSBcbntcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8OTsgaSsrKSBcbiAgICB7XG4gICAgICAgICAgaWYoaSUzPT0wKVxuICAgICAgICAgICQoXCIjcGFja2FnZV9cIitpKS5hZGRDbGFzcyhcImNhcmRTZXQwXCIpO1xuICAgICAgXG4gICAgICAgICAgaWYoaSUzPT0xKVxuICAgICAgICAgICQoXCIjcGFja2FnZV9cIitpKS5hZGRDbGFzcyhcImNhcmRTZXQxXCIpO1xuICAgICAgXG4gICAgICAgICAgIGlmKGklMz09MilcbiAgICAgICAgICAkKFwiI3BhY2thZ2VfXCIraSkuYWRkQ2xhc3MoXCJjYXJkU2V0MlwiKTtcblxuICAgICAgXG4gICAgfVxuICAgXG5cbn0pO1xuIiwiJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKVxue1xuICAgIFxuICAgICAgICAkKFwiLmNhcmQtdGV4dFwiKS5hZGRDbGFzcyhcInRleHQtanVzdGlmeVwiKTtcbiAgICBcbiAgICBcbiAgICBcbn0pOyJdfQ==

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFhX293bF9jYXJvdXNlbC9vd2xfY2Fyb3VzZWwuanMiLCJhYl9TbGlkZV9Gcm9tX0JvdHRvbS9zbGlkZV9ib3R0b20uanMiLCJhY19tb3ZlX2FuaW0vbW92ZV9hbmltLmpzIiwiYWRfZ29vZ2xlX21hcC9nb29nbGUtbWFwLmpzIiwiYWVfbW9iaWxlX21lbnUvbWVudV9tb2JpbGUuanMiLCJhZl9zdWJzY3JpcHRpb24vc3Vic2NyaXB0aW9uLmpzIiwiYWdfY29udGFjdC9jb250YWN0LmpzIiwiYWhfd2F5YW5hZC93YXlhbmFkLmpzIiwiYWlfcGFja2FnZS9wYWNrYWdlLmpzIiwiYWlfdGV4dGJhbGFuY2UvYmFsYW5jZS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUM3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDWEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDaENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDcktBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUN6QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3RFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNqS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ0pBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDbEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoic2NyaXB0X2hvbWUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgIFxuICAkKCcub3dsLWNhcm91c2VsJykub3dsQ2Fyb3VzZWwoe1xuICBcbiAgICAgLy8gQ1NTIFN0eWxlc1xuICAgIGJhc2VDbGFzcyA6IFwib3dsLWNhcm91c2VsXCIsXG4gICAgdGhlbWUgOiBcIm93bC10aGVtZVwiLFxuICAgIGxvb3A6dHJ1ZSxcbiAgICBhdXRvcGxheTp0cnVlLCAgICAgICAgXG4gICAgbWFyZ2luOjEwLFxuICAgIHJlc3BvbnNpdmVDbGFzczp0cnVlLFxuICAgIG5hdjp0cnVlLFxuICAgIHJld2luZDp0cnVlLFxuICAgIHJlc3BvbnNpdmU6e1xuICAgICAgICAwOntcbiAgICAgICAgICAgIGl0ZW1zOjEsXG4gICAgICAgICAgICBuYXY6dHJ1ZVxuICAgICAgICB9LFxuICAgICAgICA2MDA6e1xuICAgICAgICAgICAgaXRlbXM6MyxcbiAgICAgICAgICAgIG5hdjpmYWxzZVxuICAgICAgICB9LFxuICAgICAgICAxMDAwOntcbiAgICAgICAgICAgIGl0ZW1zOjMsXG4gICAgICAgICAgICBuYXY6dHJ1ZSxcbiAgICAgICAgICAgIGxvb3A6ZmFsc2VcbiAgICAgICAgfVxuICAgIH1cbn0pO1xuXG4iLCIgICQod2luZG93KS5zY3JvbGwoZnVuY3Rpb24oKSB7XG4gICQoXCIuc2xpZGVhbmltXCIpLmVhY2goZnVuY3Rpb24oKXtcbiAgICB2YXIgcG9zID0gJCh0aGlzKS5vZmZzZXQoKS50b3A7XG5cbiAgICB2YXIgd2luVG9wID0gJCh3aW5kb3cpLnNjcm9sbFRvcCgpO1xuICAgIGlmIChwb3MgPCB3aW5Ub3AgKyA2MDApIHtcbiAgICAgICQodGhpcykuYWRkQ2xhc3MoXCJzbGlkZVwiKTtcbiAgICB9XG4gIH0pO1xufSk7ICAgICAgICAgXG4gXG4iLCIkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKVxue1xuICAgIC8vIEFkZCBzbW9vdGggc2Nyb2xsaW5nIHRvIGFsbCBsaW5rcyBpbiBuYXZiYXIgKyBmb290ZXIgbGlua1xuICAgICQoXCIubmF2YmFyIGEsI215Rm9vdGVyIGFbaHJlZj0nI3BhY2thZ2UnXSwjbXlGb290ZXIgYVtocmVmPScjY29udGFjdCddLCNteUZvb3RlciBhW2hyZWY9JyN0b3BuYXZpZCddLCAjbXlGb290ZXIgYVtocmVmPScjd2F5YW5hZCddLCAjbXlGb290ZXIgYVtocmVmPScjcHJvZmlsZSddIFwiKS5vbignY2xpY2snLCBmdW5jdGlvbiAoZXZlbnQpIHtcblxuICAgICAgICAvLyBNYWtlIHN1cmUgdGhpcy5oYXNoIGhhcyBhIHZhbHVlIGJlZm9yZSBvdmVycmlkaW5nIGRlZmF1bHQgYmVoYXZpb3JcbiAgICAgICAgaWYgKHRoaXMuaGFzaCAhPT0gXCJcIilcbiAgICAgICAge1xuXG4gICAgICAgICAgICAvLyBQcmV2ZW50IGRlZmF1bHQgYW5jaG9yIGNsaWNrIGJlaGF2aW9yXG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgICAvLyBTdG9yZSBoYXNoXG4gICAgICAgICAgICB2YXIgaGFzaCA9IHRoaXMuaGFzaDtcblxuICAgICAgICAgICAgLy8gVXNpbmcgalF1ZXJ5J3MgYW5pbWF0ZSgpIG1ldGhvZCB0byBhZGQgc21vb3RoIHBhZ2Ugc2Nyb2xsXG4gICAgICAgICAgICAvLyBUaGUgb3B0aW9uYWwgbnVtYmVyICg5MDApIHNwZWNpZmllcyB0aGUgbnVtYmVyIG9mIG1pbGxpc2Vjb25kcyBpdCB0YWtlcyB0byBzY3JvbGwgdG8gdGhlIHNwZWNpZmllZCBhcmVhXG4gICAgICAgICAgICAkKCdodG1sLCBib2R5JykuYW5pbWF0ZSh7XG4gICAgICAgICAgICAgICAgc2Nyb2xsVG9wOiAkKGhhc2gpLm9mZnNldCgpLnRvcFxuICAgICAgICAgICAgfSwgOTAwLCBmdW5jdGlvbiAoKSB7XG5cbiAgICAgICAgICAgICAgICAvLyBBZGQgaGFzaCAoIykgdG8gVVJMIHdoZW4gZG9uZSBzY3JvbGxpbmcgKGRlZmF1bHQgY2xpY2sgYmVoYXZpb3IpXG4gICAgICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhhc2ggPSBoYXNoO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0gLy8gRW5kIGlmXG4gICAgfSk7XG5cblxuXG5cblxuXG59KTsiLCJcbiBcblxuXG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpXG57XG5cblxuICAgIGZ1bmN0aW9uIG15TWFwKCkge1xuXG4gICAgICAgIHZhciBteUNlbnRlciA9IG5ldyBnb29nbGUubWFwcy5MYXRMbmcoMjEuNjcwMCwgODM1Ljk1NzgpO1xuICAgICAgICB2YXIgbWFwUHJvcCA9IHtjZW50ZXI6IG15Q2VudGVyLCB6b29tOiAxMiwgc2Nyb2xsd2hlZWw6IGZhbHNlLCBkcmFnZ2FibGU6IGZhbHNlLCBtYXBUeXBlSWQ6IGdvb2dsZS5tYXBzLk1hcFR5cGVJZC5ST0FETUFQfTtcbiAgICAgICAgdmFyIG1hcCA9IG5ldyBnb29nbGUubWFwcy5NYXAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJtYXBcIiksIG1hcFByb3ApO1xuICAgICAgICB2YXIgbWFya2VyID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7cG9zaXRpb246IG15Q2VudGVyfSk7XG4gICAgICAgIG1hcmtlci5zZXRNYXAobWFwKTtcbiAgICB9XG5cbiAgICBnb29nbGUubWFwcy5ldmVudC5hZGREb21MaXN0ZW5lcih3aW5kb3csICdsb2FkJywgaW5pdE1hcCwge3Bhc3NpdmU6IHRydWV9KTtcblxuXG5cblxufSk7XG5cbiBcbmZ1bmN0aW9uIGluaXRNYXAoKSB7XG5cbiAgICAvLyBDcmVhdGUgYSBuZXcgU3R5bGVkTWFwVHlwZSBvYmplY3QsIHBhc3NpbmcgaXQgYW4gYXJyYXkgb2Ygc3R5bGVzLFxuICAgIC8vIGFuZCB0aGUgbmFtZSB0byBiZSBkaXNwbGF5ZWQgb24gdGhlIG1hcCB0eXBlIGNvbnRyb2wuXG4gICAgdmFyIHN0eWxlZE1hcFR5cGUgPSBuZXcgZ29vZ2xlLm1hcHMuU3R5bGVkTWFwVHlwZShcbiAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICB7ZWxlbWVudFR5cGU6ICdnZW9tZXRyeScsIHN0eWxlcnM6IFt7Y29sb3I6ICcjZWJlM2NkJ31dfSxcbiAgICAgICAgICAgICAgICB7ZWxlbWVudFR5cGU6ICdsYWJlbHMudGV4dC5maWxsJywgc3R5bGVyczogW3tjb2xvcjogJyM1MjM3MzUnfV19LFxuICAgICAgICAgICAgICAgIHtlbGVtZW50VHlwZTogJ2xhYmVscy50ZXh0LnN0cm9rZScsIHN0eWxlcnM6IFt7Y29sb3I6ICcjZjVmMWU2J31dfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAnYWRtaW5pc3RyYXRpdmUnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5LnN0cm9rZScsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjYzliMmE2J31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAnYWRtaW5pc3RyYXRpdmUubGFuZF9wYXJjZWwnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5LnN0cm9rZScsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjZGNkMmJlJ31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAnYWRtaW5pc3RyYXRpdmUubGFuZF9wYXJjZWwnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2xhYmVscy50ZXh0LmZpbGwnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnI2FlOWU5MCd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ2xhbmRzY2FwZS5uYXR1cmFsJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeScsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjZGZkMmFlJ31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAncG9pJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeScsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjZGZkMmFlJ31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAncG9pJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdsYWJlbHMudGV4dC5maWxsJyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyM5MzgxN2MnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdwb2kucGFyaycsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnZ2VvbWV0cnkuZmlsbCcsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjYTViMDc2J31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAncG9pLnBhcmsnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2xhYmVscy50ZXh0LmZpbGwnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnIzQ0NzUzMCd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3JvYWQnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5JyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNmNWYxZTYnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdyb2FkLmFydGVyaWFsJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeScsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjZmRmY2Y4J31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAncm9hZC5oaWdod2F5JyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeScsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjZjhjOTY3J31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAncm9hZC5oaWdod2F5JyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeS5zdHJva2UnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnI2U5YmM2Mid9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3JvYWQuaGlnaHdheS5jb250cm9sbGVkX2FjY2VzcycsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnZ2VvbWV0cnknLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnI2U5OGQ1OCd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3JvYWQuaGlnaHdheS5jb250cm9sbGVkX2FjY2VzcycsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnZ2VvbWV0cnkuc3Ryb2tlJyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNkYjg1NTUnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdyb2FkLmxvY2FsJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdsYWJlbHMudGV4dC5maWxsJyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyM4MDZiNjMnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICd0cmFuc2l0LmxpbmUnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5JyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNkZmQyYWUnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICd0cmFuc2l0LmxpbmUnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2xhYmVscy50ZXh0LmZpbGwnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnIzhmN2Q3Nyd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3RyYW5zaXQubGluZScsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnbGFiZWxzLnRleHQuc3Ryb2tlJyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNlYmUzY2QnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICd0cmFuc2l0LnN0YXRpb24nLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5JyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNkZmQyYWUnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICd3YXRlcicsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnZ2VvbWV0cnkuZmlsbCcsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjYjlkM2MyJ31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAnd2F0ZXInLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2xhYmVscy50ZXh0LmZpbGwnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnIzkyOTk4ZCd9XVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICB7bmFtZTogJ1N0eWxlZCBNYXAnfSk7XG5cbiAgICAvLyBDcmVhdGUgYSBtYXAgb2JqZWN0LCBhbmQgaW5jbHVkZSB0aGUgTWFwVHlwZUlkIHRvIGFkZFxuICAgIC8vIHRvIHRoZSBtYXAgdHlwZSBjb250cm9sLlxuICAgIHZhciBtYXAgPSBuZXcgZ29vZ2xlLm1hcHMuTWFwKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdtYXAnKSwge1xuICAgICAgICBjZW50ZXI6IHtsYXQ6IDExLjY3MDAsIGxuZzogNzUuOTU3OH0sXG4gICAgICAgIHpvb206IDExLFxuICAgICAgICBtYXBUeXBlQ29udHJvbE9wdGlvbnM6IHtcbiAgICAgICAgICAgIG1hcFR5cGVJZHM6IFsncm9hZG1hcCcsICdzYXRlbGxpdGUnLCAnaHlicmlkJywgJ3RlcnJhaW4nLFxuICAgICAgICAgICAgICAgICdzdHlsZWRfbWFwJ11cbiAgICAgICAgfVxuICAgIH0pO1xuXG4gICAgLy9Bc3NvY2lhdGUgdGhlIHN0eWxlZCBtYXAgd2l0aCB0aGUgTWFwVHlwZUlkIGFuZCBzZXQgaXQgdG8gZGlzcGxheS5cbiAgICBtYXAubWFwVHlwZXMuc2V0KCdzdHlsZWRfbWFwJywgc3R5bGVkTWFwVHlwZSk7XG4gICAgbWFwLnNldE1hcFR5cGVJZCgnc3R5bGVkX21hcCcpO1xuXG4gICAgdmFyIG15Q2VudGVyID0gbmV3IGdvb2dsZS5tYXBzLkxhdExuZygxMS42NzAwLCA3NS45NTc4KTtcbiAgICB2YXIgbWFya2VyID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7cG9zaXRpb246IG15Q2VudGVyfSk7XG4gICAgbWFya2VyLnNldE1hcChtYXApO1xuICAgIFxuICAgICBcblxuICAgIFxufSIsIiAgICAkKCcubmF2YmFyLW5hdj5saT5hJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKVxuICAgIHtcbiAgICAgICAgICAgdmFyIGlkID0gJCh0aGlzKS5hdHRyKCdpZCcpO1xuICAgICAgICAgICAvLyBhbGVydChpZCk7XG4gICAgICAgICAgIGlmKGlkICE9IFwiYWRtaW5fbmF2ZHJvcFwiKVxuICAgICAgICAgICAgICAgICAgJCgnLm5hdmJhci1jb2xsYXBzZScpLmNvbGxhcHNlKCdoaWRlJyk7XG4gICAgfSk7XG4gICAgXG4gICAgJCgnLm5hdmJhci1jb2xsYXBzZScpLm9uKCdzaG93bi5icy5jb2xsYXBzZScsIGZ1bmN0aW9uKClcbiAgICB7XG4gICAgICAgLy9hbGVydChcImhlbGxvXCIpO1xuICAgICAgICAkKCcjdGFnJykuaHRtbChcIk1lbnVcIik7XG4gICAgICAgICQoJyN0YWcnKS5yZW1vdmVDbGFzcyhcImZhIGZhLWJhcnNcIik7XG4gICAgICAgICQoJyN0YWcnKS5hZGRDbGFzcyhcImZhIGZhLXRpbWVzXCIpO1xuICAgICAgICBcbiAgICAgICBcbiAgICB9KTtcbiAgICBcbiAgICBcbiAgICAkKCcubmF2YmFyLWNvbGxhcHNlJykub24oJ2hpZGRlbi5icy5jb2xsYXBzZScsIGZ1bmN0aW9uKClcbiAgICB7XG4gICAgICAvLyAkKCcjdGFnJykuaHRtbChcIk9cIik7XG4gICAgICAgICAgICAgJCgnI3RhZycpLmh0bWwoXCJNZW51XCIpO1xuICAgICAgICAgICAgJCgnI3RhZycpLnJlbW92ZUNsYXNzKFwiZmEgZmEtdGltZXNcIik7XG4gICAgICAgICAgICAgICQoJyN0YWcnKS5hZGRDbGFzcyhcImZhIGZhLWJhcnNcIik7XG4gICAgfSk7IiwiLy8gbWFnaWMuanNcbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcblxuICAgIC8vIHByb2Nlc3MgdGhlIGZvcm1cbiAgICAkKCcjZm9ybXN1YicpLnN1Ym1pdChmdW5jdGlvbiAoZXZlbnQpIHtcblxuICAgICAgICAvLyBnZXQgdGhlIGZvcm0gZGF0YVxuICAgICAgICAvLyB0aGVyZSBhcmUgbWFueSB3YXlzIHRvIGdldCB0aGlzIGRhdGEgdXNpbmcgalF1ZXJ5ICh5b3UgY2FuIHVzZSB0aGUgY2xhc3Mgb3IgaWQgYWxzbylcbiAgICAgICAgdmFyIGZvcm1EYXRhID0ge1xuICAgICAgICAgLy8gICAnbmFtZSc6ICQoJ2lucHV0W25hbWU9bmFtZV0nKS52YWwoKSxcbiAgICAgICAgICAgICdlbWFpbCc6ICQoJ2lucHV0W25hbWU9ZW1haWxdJykudmFsKCksXG4gICAgICAgICAgLy8gICdzdXBlcmhlcm9BbGlhcyc6ICQoJ2lucHV0W25hbWU9c3VwZXJoZXJvQWxpYXNdJykudmFsKClcbiAgICAgICAgfTtcblxuICAgICAgICAvLyBwcm9jZXNzIHRoZSBmb3JtXG4gICAgICAgICQuYWpheCh7XG4gICAgICAgICAgICB0eXBlOiAnUE9TVCcsIC8vIGRlZmluZSB0aGUgdHlwZSBvZiBIVFRQIHZlcmIgd2Ugd2FudCB0byB1c2UgKFBPU1QgZm9yIG91ciBmb3JtKVxuICAgICAgICAgICAgdXJsOiAnL3N1YnMnLCAvLyB0aGUgdXJsIHdoZXJlIHdlIHdhbnQgdG8gUE9TVFxuICAgICAgICAgICAgZGF0YTogZm9ybURhdGEsIC8vIG91ciBkYXRhIG9iamVjdFxuICAgICAgICAgICAgZGF0YVR5cGU6ICdqc29uJywgLy8gd2hhdCB0eXBlIG9mIGRhdGEgZG8gd2UgZXhwZWN0IGJhY2sgZnJvbSB0aGUgc2VydmVyXG4gICAgICAgICAgICBlbmNvZGU6IHRydWVcbiAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAvLyB1c2luZyB0aGUgZG9uZSBwcm9taXNlIGNhbGxiYWNrXG4gICAgICAgICAgICAgICAgLmRvbmUoZnVuY3Rpb24gKGRhdGEpIHtcblxuICAgICAgICAgICAgICAgICAgICAvLyBsb2cgZGF0YSB0byB0aGUgY29uc29sZSBzbyB3ZSBjYW4gc2VlXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpO1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIGhlcmUgd2Ugd2lsbCBoYW5kbGUgZXJyb3JzIGFuZCB2YWxpZGF0aW9uIG1lc3NhZ2VzXG4gICAgICAgICAgICAgICAgICAgIGlmICghZGF0YS5zdWNjZXNzKSBcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjc3VibXNnXCIpLmh0bWwoZGF0YS5lcnJvcnMuZW1haWwpO1xuLyogICAgICAgICAgICAgICAgICAgICAgICAvLyBoYW5kbGUgZXJyb3JzIGZvciBuYW1lIC0tLS0tLS0tLS0tLS0tLVxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEuZXJyb3JzLm5hbWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjbmFtZS1ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTsgLy8gYWRkIHRoZSBlcnJvciBjbGFzcyB0byBzaG93IHJlZCBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNuYW1lLWdyb3VwJykuYXBwZW5kKCc8ZGl2IGNsYXNzPVwiaGVscC1ibG9ja1wiPicgKyBkYXRhLmVycm9ycy5uYW1lICsgJzwvZGl2PicpOyAvLyBhZGQgdGhlIGFjdHVhbCBlcnJvciBtZXNzYWdlIHVuZGVyIG91ciBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBoYW5kbGUgZXJyb3JzIGZvciBlbWFpbCAtLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLmVycm9ycy5lbWFpbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNlbWFpbC1ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTsgLy8gYWRkIHRoZSBlcnJvciBjbGFzcyB0byBzaG93IHJlZCBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNlbWFpbC1ncm91cCcpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImhlbHAtYmxvY2tcIj4nICsgZGF0YS5lcnJvcnMuZW1haWwgKyAnPC9kaXY+Jyk7IC8vIGFkZCB0aGUgYWN0dWFsIGVycm9yIG1lc3NhZ2UgdW5kZXIgb3VyIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBlcnJvcnMgZm9yIHN1cGVyaGVybyBhbGlhcyAtLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLmVycm9ycy5zdXBlcmhlcm9BbGlhcykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNzdXBlcmhlcm8tZ3JvdXAnKS5hZGRDbGFzcygnaGFzLWVycm9yJyk7IC8vIGFkZCB0aGUgZXJyb3IgY2xhc3MgdG8gc2hvdyByZWQgaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjc3VwZXJoZXJvLWdyb3VwJykuYXBwZW5kKCc8ZGl2IGNsYXNzPVwiaGVscC1ibG9ja1wiPicgKyBkYXRhLmVycm9ycy5zdXBlcmhlcm9BbGlhcyArICc8L2Rpdj4nKTsgLy8gYWRkIHRoZSBhY3R1YWwgZXJyb3IgbWVzc2FnZSB1bmRlciBvdXIgaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgIH0qL1xuXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIEFMTCBHT09EISBqdXN0IHNob3cgdGhlIHN1Y2Nlc3MgbWVzc2FnZSFcbiAgICAgICAgICAgICAgICAgICAgICAgLy8gJCgnZm9ybScpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImFsZXJ0IGFsZXJ0LXN1Y2Nlc3NcIj4nICsgZGF0YS5tZXNzYWdlICsgJzwvZGl2PicpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB1c3VhbGx5IGFmdGVyIGZvcm0gc3VibWlzc2lvbiwgeW91J2xsIHdhbnQgdG8gcmVkaXJlY3RcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHdpbmRvdy5sb2NhdGlvbiA9ICcvdGhhbmsteW91JzsgLy8gcmVkaXJlY3QgYSB1c2VyIHRvIGFub3RoZXIgcGFnZVxuICAgICAgICAgICAgICAgICAgICAgIC8vICBhbGVydCgnc3VjY2VzcycrZGF0YS5tZXNzYWdlKTsgLy8gZm9yIG5vdyB3ZSdsbCBqdXN0IGFsZXJ0IHRoZSB1c2VyXG4gICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgJChcIiNzdWJtc2dcIikuaHRtbChkYXRhLm1lc3NhZ2UpO1xuXG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgIC8vIHN0b3AgdGhlIGZvcm0gZnJvbSBzdWJtaXR0aW5nIHRoZSBub3JtYWwgd2F5IGFuZCByZWZyZXNoaW5nIHRoZSBwYWdlXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgfSk7XG5cbn0pOyIsIi8vIG1hZ2ljLmpzXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XG5cbiAgICAvLyBwcm9jZXNzIHRoZSBmb3JtXG4gICAgJCgnI2NvbnRhY3RJZCcpLnN1Ym1pdChmdW5jdGlvbiAoZXZlbnQpIHtcblxuICAgICAgICAvLyBnZXQgdGhlIGZvcm0gZGF0YVxuICAgICAgICAvLyB0aGVyZSBhcmUgbWFueSB3YXlzIHRvIGdldCB0aGlzIGRhdGEgdXNpbmcgalF1ZXJ5ICh5b3UgY2FuIHVzZSB0aGUgY2xhc3Mgb3IgaWQgYWxzbylcbiAgICAgICAgdmFyIGZvcm1EYXRhID0ge1xuICAgICAgICAgICAgJ2FtZSc6ICQoJ2lucHV0W25hbWU9bmFtZV0nKS52YWwoKSxcbiAgICAgICAgICAgICdlbWFpbCc6ICQoJ2lucHV0W25hbWU9ZW1haWxdJykudmFsKCksXG4gICAgICAgICAgICAnY29tbWVudHMnOiAkKCdpbnB1dFtuYW1lPWNvbW1lbnRzXScpLnZhbCgpXG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gcHJvY2VzcyB0aGUgZm9ybVxuICAgICAgICAkLmFqYXgoe1xuICAgICAgICAgICAgdHlwZTogJ1BPU1QnLCAvLyBkZWZpbmUgdGhlIHR5cGUgb2YgSFRUUCB2ZXJiIHdlIHdhbnQgdG8gdXNlIChQT1NUIGZvciBvdXIgZm9ybSlcbiAgICAgICAgICAgIHVybDogJy9jb250YWN0JywgLy8gdGhlIHVybCB3aGVyZSB3ZSB3YW50IHRvIFBPU1RcbiAgICAgICAgICAgIGRhdGE6IGZvcm1EYXRhLCAvLyBvdXIgZGF0YSBvYmplY3RcbiAgICAgICAgICAgIGRhdGFUeXBlOiAnanNvbicsIC8vIHdoYXQgdHlwZSBvZiBkYXRhIGRvIHdlIGV4cGVjdCBiYWNrIGZyb20gdGhlIHNlcnZlclxuICAgICAgICAgICAgZW5jb2RlOiB0cnVlXG4gICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLy8gdXNpbmcgdGhlIGRvbmUgcHJvbWlzZSBjYWxsYmFja1xuICAgICAgICAgICAgICAgIC5kb25lKGZ1bmN0aW9uIChkYXRhKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gbG9nIGRhdGEgdG8gdGhlIGNvbnNvbGUgc28gd2UgY2FuIHNlZVxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcblxuICAgICAgICAgICAgICAgICAgICAvLyBoZXJlIHdlIHdpbGwgaGFuZGxlIGVycm9ycyBhbmQgdmFsaWRhdGlvbiBtZXNzYWdlc1xuICAgICAgICAgICAgICAgICAgICBpZiAoIWRhdGEuc3VjY2VzcylcbiAgICAgICAgICAgICAgICAgICAge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAkKFwiI2NvbnRhY3Rtc2dcIikuaHRtbChkYXRhLmVycm9ycy5lbWFpbCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAvKiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBlcnJvcnMgZm9yIG5hbWUgLS0tLS0tLS0tLS0tLS0tXG4gICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEuZXJyb3JzLm5hbWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjbmFtZS1ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTsgLy8gYWRkIHRoZSBlcnJvciBjbGFzcyB0byBzaG93IHJlZCBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNuYW1lLWdyb3VwJykuYXBwZW5kKCc8ZGl2IGNsYXNzPVwiaGVscC1ibG9ja1wiPicgKyBkYXRhLmVycm9ycy5uYW1lICsgJzwvZGl2PicpOyAvLyBhZGQgdGhlIGFjdHVhbCBlcnJvciBtZXNzYWdlIHVuZGVyIG91ciBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgICAvLyBoYW5kbGUgZXJyb3JzIGZvciBlbWFpbCAtLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5lcnJvcnMuZW1haWwpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjZW1haWwtZ3JvdXAnKS5hZGRDbGFzcygnaGFzLWVycm9yJyk7IC8vIGFkZCB0aGUgZXJyb3IgY2xhc3MgdG8gc2hvdyByZWQgaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjZW1haWwtZ3JvdXAnKS5hcHBlbmQoJzxkaXYgY2xhc3M9XCJoZWxwLWJsb2NrXCI+JyArIGRhdGEuZXJyb3JzLmVtYWlsICsgJzwvZGl2PicpOyAvLyBhZGQgdGhlIGFjdHVhbCBlcnJvciBtZXNzYWdlIHVuZGVyIG91ciBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgICAvLyBoYW5kbGUgZXJyb3JzIGZvciBzdXBlcmhlcm8gYWxpYXMgLS0tLS0tLS0tLS0tLS0tXG4gICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEuZXJyb3JzLnN1cGVyaGVyb0FsaWFzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgJCgnI3N1cGVyaGVyby1ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTsgLy8gYWRkIHRoZSBlcnJvciBjbGFzcyB0byBzaG93IHJlZCBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNzdXBlcmhlcm8tZ3JvdXAnKS5hcHBlbmQoJzxkaXYgY2xhc3M9XCJoZWxwLWJsb2NrXCI+JyArIGRhdGEuZXJyb3JzLnN1cGVyaGVyb0FsaWFzICsgJzwvZGl2PicpOyAvLyBhZGQgdGhlIGFjdHVhbCBlcnJvciBtZXNzYWdlIHVuZGVyIG91ciBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgIH0qL1xuXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIEFMTCBHT09EISBqdXN0IHNob3cgdGhlIHN1Y2Nlc3MgbWVzc2FnZSFcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vICQoJ2Zvcm0nKS5hcHBlbmQoJzxkaXYgY2xhc3M9XCJhbGVydCBhbGVydC1zdWNjZXNzXCI+JyArIGRhdGEubWVzc2FnZSArICc8L2Rpdj4nKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gdXN1YWxseSBhZnRlciBmb3JtIHN1Ym1pc3Npb24sIHlvdSdsbCB3YW50IHRvIHJlZGlyZWN0XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB3aW5kb3cubG9jYXRpb24gPSAnL3RoYW5rLXlvdSc7IC8vIHJlZGlyZWN0IGEgdXNlciB0byBhbm90aGVyIHBhZ2VcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vICBhbGVydCgnc3VjY2VzcycrZGF0YS5tZXNzYWdlKTsgLy8gZm9yIG5vdyB3ZSdsbCBqdXN0IGFsZXJ0IHRoZSB1c2VyXG5cbiAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjY29udGFjdG1zZ1wiKS5odG1sKGRhdGEubWVzc2FnZSk7XG5cbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgLy8gc3RvcCB0aGUgZm9ybSBmcm9tIHN1Ym1pdHRpbmcgdGhlIG5vcm1hbCB3YXkgYW5kIHJlZnJlc2hpbmcgdGhlIHBhZ2VcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICB9KTtcblxuXG5cbiAgICAkKCcjZW5xdWlyeWZvcm0nKS5zdWJtaXQoZnVuY3Rpb24gKGV2ZW50KSB7XG5cbiAgICAgLy8gIHZhciByZXN1bHQgPSAkKCcjZW5xaWQnKS52YWwoKTtcbiAgICAgICB2YXIgbmFtZSA9ICQoJyNuYW1lJykudmFsKCk7XG4gICAgICAgdmFyIGVtYWlsPSQoJyNlbWFpbCcpLnZhbCgpO1xuICAgICAgIHZhciBtb2JpbGU9JCgnI21vYmlsZScpLnZhbCgpO1xuICAgICAgIHZhciBjb21tZW50cz0kKCcjY29tbWVudHMnKS52YWwoKTtcbiAgICAgXG4gICAgICAgXG4gICAgICAgLy8gYWxlcnQoXCJoYWlcIik7XG5cbiAgICAgICAgLy8gZ2V0IHRoZSBmb3JtIGRhdGFcbiAgICAgICAgLy8gdGhlcmUgYXJlIG1hbnkgd2F5cyB0byBnZXQgdGhpcyBkYXRhIHVzaW5nIGpRdWVyeSAoeW91IGNhbiB1c2UgdGhlIGNsYXNzIG9yIGlkIGFsc28pXG4gICAgICAgIHZhciBmb3JtRGF0YSA9IHtcbiAgICAgICAgICAgICAnbmFtZSc6IG5hbWUsXG4gICAgICAgICAgICAgJ2VtYWlsJzogZW1haWwsXG4gICAgICAgICAgICdjb21tZW50cyc6IGNvbW1lbnRzLFxuICAgICAgICAgICAgICdtb2JpbGUnICA6bW9iaWxlXG4gICAgICAgIH07XG4gICAgICAgLy8gY29uc29sZS5sb2coXCJIZWxsbyA6OiBcIitmb3JtRGF0YSk7XG4gICAgICAgLy8gXG4gICAgICAgLy8gXG4gICAgICAgIC8vIHByb2Nlc3MgdGhlIGZvcm1cbiAgICAgICAgXG4gICAgICAgLy8gYWxlcnQoZm9ybURhdGEubmFtZStcIjo6XCIrZm9ybURhdGEuZW1haWwgK1wiOjpcIitmb3JtRGF0YS5tb2JpbGUrXCI6OlwiK2Zvcm1EYXRhLmNvbW1lbnRzKTtcbiAgICAgICAgJC5hamF4KHtcbiAgICAgICAgICAgIHR5cGU6ICdQT1NUJywgLy8gZGVmaW5lIHRoZSB0eXBlIG9mIEhUVFAgdmVyYiB3ZSB3YW50IHRvIHVzZSAoUE9TVCBmb3Igb3VyIGZvcm0pXG4gICAgICAgICAgICB1cmw6ICcvY29udGFjdCcsIC8vIHRoZSB1cmwgd2hlcmUgd2Ugd2FudCB0byBQT1NUXG4gICAgICAgICAgICBkYXRhOiBmb3JtRGF0YSwgLy8gb3VyIGRhdGEgb2JqZWN0XG4gICAgICAgICAgICBkYXRhVHlwZTogJ2pzb24nLCAvLyB3aGF0IHR5cGUgb2YgZGF0YSBkbyB3ZSBleHBlY3QgYmFjayBmcm9tIHRoZSBzZXJ2ZXJcbiAgICAgICAgICAgIGVuY29kZTogdHJ1ZVxuICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC8vIHVzaW5nIHRoZSBkb25lIHByb21pc2UgY2FsbGJhY2tcbiAgICAgICAgICAgICAgICAuZG9uZShmdW5jdGlvbiAoZGF0YSkge1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIGxvZyBkYXRhIHRvIHRoZSBjb25zb2xlIHNvIHdlIGNhbiBzZWVcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YSk7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gaGVyZSB3ZSB3aWxsIGhhbmRsZSBlcnJvcnMgYW5kIHZhbGlkYXRpb24gbWVzc2FnZXNcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFkYXRhLnN1Y2Nlc3MpXG4gICAgICAgICAgICAgICAgICAgIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNjb250YWN0bXNnXCIpLmh0bWwoZGF0YS5tZXNzYWdlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qICAgICAgICAgICAgICAgICAgICAgICAgLy8gaGFuZGxlIGVycm9ycyBmb3IgbmFtZSAtLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5lcnJvcnMubmFtZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNuYW1lLWdyb3VwJykuYWRkQ2xhc3MoJ2hhcy1lcnJvcicpOyAvLyBhZGQgdGhlIGVycm9yIGNsYXNzIHRvIHNob3cgcmVkIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgJCgnI25hbWUtZ3JvdXAnKS5hcHBlbmQoJzxkaXYgY2xhc3M9XCJoZWxwLWJsb2NrXCI+JyArIGRhdGEuZXJyb3JzLm5hbWUgKyAnPC9kaXY+Jyk7IC8vIGFkZCB0aGUgYWN0dWFsIGVycm9yIG1lc3NhZ2UgdW5kZXIgb3VyIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBlcnJvcnMgZm9yIGVtYWlsIC0tLS0tLS0tLS0tLS0tLVxuICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLmVycm9ycy5lbWFpbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNlbWFpbC1ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTsgLy8gYWRkIHRoZSBlcnJvciBjbGFzcyB0byBzaG93IHJlZCBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNlbWFpbC1ncm91cCcpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImhlbHAtYmxvY2tcIj4nICsgZGF0YS5lcnJvcnMuZW1haWwgKyAnPC9kaXY+Jyk7IC8vIGFkZCB0aGUgYWN0dWFsIGVycm9yIG1lc3NhZ2UgdW5kZXIgb3VyIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBlcnJvcnMgZm9yIHN1cGVyaGVybyBhbGlhcyAtLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5lcnJvcnMuc3VwZXJoZXJvQWxpYXMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjc3VwZXJoZXJvLWdyb3VwJykuYWRkQ2xhc3MoJ2hhcy1lcnJvcicpOyAvLyBhZGQgdGhlIGVycm9yIGNsYXNzIHRvIHNob3cgcmVkIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgJCgnI3N1cGVyaGVyby1ncm91cCcpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImhlbHAtYmxvY2tcIj4nICsgZGF0YS5lcnJvcnMuc3VwZXJoZXJvQWxpYXMgKyAnPC9kaXY+Jyk7IC8vIGFkZCB0aGUgYWN0dWFsIGVycm9yIG1lc3NhZ2UgdW5kZXIgb3VyIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgfSovXG5cbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gQUxMIEdPT0QhIGp1c3Qgc2hvdyB0aGUgc3VjY2VzcyBtZXNzYWdlIVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gJCgnZm9ybScpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImFsZXJ0IGFsZXJ0LXN1Y2Nlc3NcIj4nICsgZGF0YS5tZXNzYWdlICsgJzwvZGl2PicpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB1c3VhbGx5IGFmdGVyIGZvcm0gc3VibWlzc2lvbiwgeW91J2xsIHdhbnQgdG8gcmVkaXJlY3RcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHdpbmRvdy5sb2NhdGlvbiA9ICcvdGhhbmsteW91JzsgLy8gcmVkaXJlY3QgYSB1c2VyIHRvIGFub3RoZXIgcGFnZVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gIGFsZXJ0KCdzdWNjZXNzJytkYXRhLm1lc3NhZ2UpOyAvLyBmb3Igbm93IHdlJ2xsIGp1c3QgYWxlcnQgdGhlIHVzZXJcblxuICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNjb250YWN0bXNnXCIpLmh0bWwoZGF0YS5tZXNzYWdlKTtcblxuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAvLyBzdG9wIHRoZSBmb3JtIGZyb20gc3VibWl0dGluZyB0aGUgbm9ybWFsIHdheSBhbmQgcmVmcmVzaGluZyB0aGUgcGFnZVxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIH0pO1xuXG5cblxufSk7XG5cblxuXG5cblxuXG4iLCIvLyBBICQoIGRvY3VtZW50ICkucmVhZHkoKSBibG9jay5cbiQoIGRvY3VtZW50ICkucmVhZHkoZnVuY3Rpb24oKSB7XG4gICAgJChcIiN3YXlhbmFkXzFcIikuYWRkQ2xhc3MoXCJhY3RpdmVcIik7XG5cbn0pOyIsIiQoIGRvY3VtZW50ICkucmVhZHkoZnVuY3Rpb24oKSBcbntcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8OTsgaSsrKSBcbiAgICB7XG4gICAgICAgICAgaWYoaSUzPT0wKVxuICAgICAgICAgICQoXCIjcGFja2FnZV9cIitpKS5hZGRDbGFzcyhcImNhcmRTZXQwXCIpO1xuICAgICAgXG4gICAgICAgICAgaWYoaSUzPT0xKVxuICAgICAgICAgICQoXCIjcGFja2FnZV9cIitpKS5hZGRDbGFzcyhcImNhcmRTZXQxXCIpO1xuICAgICAgXG4gICAgICAgICAgIGlmKGklMz09MilcbiAgICAgICAgICAkKFwiI3BhY2thZ2VfXCIraSkuYWRkQ2xhc3MoXCJjYXJkU2V0MlwiKTtcblxuICAgICAgXG4gICAgfVxuICAgXG5cbn0pO1xuIiwiJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKVxue1xuICAgIFxuICAgICAgICAkKFwiLmNhcmQtdGV4dFwiKS5hZGRDbGFzcyhcInRleHQtanVzdGlmeVwiKTtcbiAgICBcbiAgICBcbiAgICBcbn0pOyJdfQ==
