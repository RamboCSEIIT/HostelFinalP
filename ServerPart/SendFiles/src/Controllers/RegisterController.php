<?php  namespace tour\Controllers;

 
 
use tour\Validation\Validator;

use duncan3dc\Laravel\BladeInstance;
use Illuminate\Database\Capsule\Manager as DB;
use tour\email_send\Semail;
 
class RegisterController extends BaseController 
{
    
  
  ///  
  
    
      
    
     public function getShowRegisterPage( ) 
    {
          // include (__DIR__."/../../Views/register_page.html");
         echo $this->blade->render('register_page', [
           
            'page_name' => '#register-page',
              'signer' => $this->signer
        ]);
    }
    
    public function postShowRegisterPage( ) 
    {
    
    /*
         if (!$this->signer->validateSignature($_POST['_token']))
        {
              $_SESSION['msg'] = ["Insecure Registration!"];
             echo $this->blade->render('register_page', [
           
            'page_name' => '#register-page',
                  'signer' => $this->signer
        ]); 
                 unset( $_SESSION['msg']);
            exit();
        }
     */   
        
        
         $errors = [];
         unset($_SESSION['msg']);
         
        $validation_data = [
          'first_name_name' => 'min:3',
          'last_name_name' => 'min:3',
          'email_name_name' => 'email|equalTo:verify_email_name_name|unique:users',
          'password_name' => 'min:3|equalTo:verify_password_name'
          
        ];

        // validate data
        $validator = new Validator();

        $errors = $validator->isValid($validation_data);
       // var_dump($errors);
        
       
          unset($_SESSION['msg']);
        
         if (sizeof($errors) > 0)
        {
           
           // dd($_SESSION['msg']);
             
              $_SESSION['msg'] =$errors;
           //   header("Location: /register");
            // Blade cant take SESSION
     
                
             echo $this->blade->render('register_page', [
           
            'page_name' => '#register-page',
                  'signer' => $this->signer
        ]); 
           
             unset( $_SESSION['msg']);
            exit();
        }

/*        
       $user = new User;
       $user->first_name               = $_REQUEST['first_name_name']; 
       $user->last_name                = $_REQUEST['last_name_name'] ;
       $user->email                    = $_REQUEST['email_name_name'];
       $user->password                 = password_hash($_REQUEST['password_name'],PASSWORD_DEFAULT);
       
       $user->save();
       echo "<br>--- posted<br>";
           
*/
        // //http://fideloper.com/laravel-raw-queries
        DB::statement('INSERT INTO users (first_name, last_name, email,password,mob_number) VALUES (:first_name, :last_name,:email,:password,:mob_number)',
                array(
                        'first_name'  => $_REQUEST['first_name_name'],
                        'last_name'   => $_REQUEST['last_name_name'],
                        'email'       => $_REQUEST['email_name_name'],
                        'password'    => password_hash($_REQUEST['password_name'],PASSWORD_DEFAULT),
                        'mob_number'  => (string)$_REQUEST['mob_number_name'])
                     );
        
         
        $row =  DB::select('SELECT id FROM users WHERE email = :email',
                                                        array(
                                                                 'email'  => $_REQUEST['email_name_name']
                                                             )
                                                  ); 
       
     /*           
       // $row =  DB::select('SELECT id FROM users'); 

        
        var_dump($row);
        echo "<br>".gettype($row);
        $temp = $row[0];
         echo "------<br>";
         $age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
         echo "Peter is " . $age['Peter'] . " years old.";
        
        
        // dd($age."<br>---<br>".$temp);
        foreach($temp as $x => $x_value) 
        {
             echo "Key=" . $x . ", Value=" . $x_value;
       
             echo "<br>";
        }
         echo "------<br>";
       // dd($temp->id);*/
          
        
        $token = md5(uniqid(rand(), true)) . md5(uniqid(rand(), true));       

        DB::statement('INSERT INTO users_pending (token, user_id) VALUES (:token, :user_id)',
                                    array(
                                            'token'     => $token ,
                                            'user_id'   => $row[0]->id
                                         )
                     );

        
        // echo "<br>--- posted<br>";
        header("Location: /success");
      

        $message = $this->blade->render('emails.welcome-email',
            ['token' => $token,
               'signer' => $this->signer]
        );
               
   
        Semail::_semail($_REQUEST['email_name_name'], "Welcome to Wayanadtours and Travels", $message);
   
        
    }
    
    
    public function getVerifyAccount()
    {
        
        echo "inside verify account";
        
        $user_id = 0;
        $token = $_GET['token'];
        
       // $token = "31cb6adfc9bcff31f8ebded5bb44ba64bae8ec535625a82b512c95b9c6a4b553";
        
        
        $results =  DB::select('SELECT user_id FROM users_pending WHERE token = :token',
                                                        array(
                                                                 'token'  =>  $token
                                                             )
                                                  );
        
        

        if(count($results)>0)
        {
               
            DB::statement('  
                
                            UPDATE users
                            SET active=:active
                            WHERE id=:user_id
                     ',
                    
                            array(
                                     'active'  =>  1,
                                     'user_id'=>$results[0]->user_id
                                      
                                 )

                    );
          
            
            DB::statement('
                                DELETE FROM users_pending
                                WHERE token = :token', 
                                array(
                                       'token'  =>  $token
                                     )
                           );
  
               header("Location: /account-activated");
                 dd( $results );
               exit();
           }
        else 
        {
                header("Location: /page-not-found");
                exit();
        }

        
         

    }

    
   
    
    
    
}

?>
