<?php namespace tour\models;
 

use Illuminate\Database\Eloquent\Model as Eloquent;

class User extends Eloquent
{
   // in our data base no created or update column 
   public  $timestamps = false;
}
 
?>