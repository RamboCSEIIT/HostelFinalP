<?php $__env->startSection('test-form'); ?>
 



    <!-- OUR FORM -->
    <form class="form-inline justify-content-center" id="formsub" action="/subs" method="POST" style="padding-left: 15px;padding-right: 15px">



        <!-- EMAIL -->
        
        <div class="input-group">
            <input  type="email" class="company_email_font form-control" size="30" name="email" placeholder="Email Address" required>
            <div class="input-group-btn">
                <button type="submit" class="btn btn-danger company_email_font">   Subscribe <span class="fa fa-arrow-right"></span>  </button>
            </div>
        </div>


  
         

    </form>
      <span id="submsg" class="company_moto_font center" style="color: whitesmoke;background-color:maroon"></span>

 
<?php $__env->stopSection(); ?>


<?php $__env->startSection('form'); ?>
<form class="form-inline justify-content-center" action="#"  onsubmit="return submitForm();" id="myform"  method="POST" style="padding-left: 15px;padding-right: 15px">
    <div class="input-group">
        <input  type="email" class="company_email_font form-control" size="30" placeholder="Email Address" required>
        <div class="input-group-btn">
            <button type="submit" class="btn btn-danger company_email_font">   Subscribe  </button>
        </div>
    </div>
</form>

<?php $__env->stopSection(); ?>



<div   id="jumbotron">





    <div class="jumbotron-fluid text-center">
        <br>
        <br>
        <br>
        <img class="logo img-circle" src="/02_IMAGES/favicon.png" alt="Logo" width="170" height="170">
        <h1 class ="company_caption_font">Company Name </h1> 
        <p class="company_moto_font" >"Make Your Next Trip Unforgettable With Us"</p> 



        <?php echo $__env->yieldContent('test-form'); ?>



        <br>
        <br>
        <br>




    </div>
</div>    
<!-- Container (Services Section) -->
<div id="icons">
    
    <div class="marquee">
        <br>
 
        <p><b> Site design is being getting updated. More options will be available in few days </b> </p>
    </div>
    <div class="container-fluid text-center">
        <div class="row">
            <br>
            <br>
        </div>
        <h2 class = "icon_font">SERVICES</h2>

        <br>
        <div class="row">
            <div class="col-sm-4">
                <i class="fa fa-columns" style="font-size:48px;color:red"></i>
                <h4 class="">HOTELS</h4>
                <p> We provide hotel booking as per the customer requirement. You can book through our site or via directly contacting our team</p>
            </div>
            <div class="col-sm-4">
                <i class="fa fa-text-width" style="font-size:48px;color:red"></i>
                <h4 class="">TOUR GUIDING</h4>
                <p> Experienced tour guide to assist your trip</p>
            </div>
            <div class="col-sm-4">
                <i class="fa fa-paragraph" style="font-size:48px;color:red"></i>
                <h4 class="">CUSTOMER CARE</h4>
                <p> We offer service which ensures customer satisfaction. Our team will be available any time including day and night on request. </p>
            </div>
        </div>
        <br><br>
        <div class="row">
            <div class="col-sm-4">
                <i class="fa fa-strikethrough" style="font-size:48px;color:red"></i>
                <h4 class="">SECURITY</h4>
                <p>Security of our customer is utmost priority to us. We make sure that apt people will be available at your service on your trip. Proper monitoring of the hotel and travel amenities is done prior to the trip and also during the tour. </p>
            </div>
            
            
             <div class="col-sm-4">
                <i class="fa fa-undo" style="font-size:48px;color:red"></i>
                <h4 class="company_moto_font">Medical Emergency </h4>
                <p> In case of medical emergencies caused by health issues we will assist you to get the best medical facilities in Wayanad . You will be provided with our medical assistance any time </p>
            </div>
           
            
            <div class="col-sm-4">
                <i class="fa fa-table" style="font-size:48px;color:red"></i>

                <h4 class="">TRANSPORTATION</h4>
                <p> It can be arranged before or after the arrival of the tour. We will arrange travel amenities with properly maintained vechicles as your security is utmost concern for us</p>
            </div>
        </div>
    </div>

</div>


<?php $__env->startSection('image_wyn'); ?> 

 
                         
                      
                        
                        <?php $__currentLoopData = $galleryWayanad; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $gallery_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        
                            <?php
                               $id = "wayanad_".$index;
                            ?>
               
                     
                            <div class="carousel-item " id = "<?php echo $id; ?>">
                        
                        
                 


                            <img src="<?php echo $gallery_item->image_link; ?>" alt="Wayanad"  >
                            <div class="carousel-caption">
                                 <h3>  Welcome to Wayanad</h3>
                                
                                                                
                            </div> 

                        </div>

                     
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


<?php $__env->stopSection(); ?> 



<div id="wayanad" style="background-color:#f6f6f6;">
    <div class="container">

        <nav id = "Wayanad" class="navbar">
            <div id="#test">

            </div>
        </nav>

        <div class="row  ">
            <br>
            <br>
            <br>
            <br>               
        </div>
        <div class="row">
            <div class="col-md-12 col-md-offset-2">
                <h2 class="font_wayanad text-center"> Welcome to Wayanad</h2><br>
                <div id="demo" class="carousel slide" data-ride="carousel">
                    <ul class="carousel-indicators">
                        <li data-target="#demo" data-slide-to="0" class="active"></li>
                        <li data-target="#demo" data-slide-to="1"></li>
                        <li data-target="#demo" data-slide-to="2"></li>
                    </ul>
                    <div class="carousel-inner">
                         <?php echo $__env->yieldContent('image_wyn'); ?>
                        
                    </div> 
                </div>
                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#demo" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
            </div>



        </div>

        <div class="row">
            <br>
            <br>

        </div>

    </div>   

</div>








<?php $__env->startSection('image_prl'); ?> 





<?php $__currentLoopData = $galleryProfile; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $gallery_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>


<div class="item"> 
    <img src="<?php echo $gallery_item->image_link; ?>" alt="Los Angeles" class="img-fluid" >
</div>








<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


<?php $__env->stopSection(); ?> 

     
    <div id="profile">


        <h2 class="font_profile text-center"> Company Welcome</h2><br>
        <div class="container mt-5">


            <div class="row">

                <div class="owl-carousel owl-theme">                    
                    <?php echo $__env->yieldContent('image_prl'); ?>                    

                </div>
            </div> 
            <div class="row">
                <br>
                <br>

            </div>


        </div>




    </div>


<?php $__env->startSection('image_pkg'); ?> 

 



<?php $__currentLoopData = $galleryPackage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $gallery_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>



   <?php if($index%3==0): ?>
           <div class="card-deck" style="margin-top: 20px">

    <?php endif; ?>


    <?php
    $id = "package_".$index;
    ?>



    <div class="card">


        <div class="card-body  text-white" id = "<?php echo $id; ?>">




            <img src="<?php echo $gallery_item->image_link; ?>" alt="Smiley face" class="img-fluid"  >
          

            <?php if( $gallery_item->tentative==1): ?>
             <h2 class="card-title mt-2 text-center" style="background: greenyellow;  border-radius: 40px; padding: 1px;font-size: 30px"><?php echo $gallery_item->heading; ?></h2>
  
            <h5 class="card-subtitle text-center"><span>&#8377;</span>  Cost Tentative            </h5>

            <?php else: ?>
                      <h2 class="card-title mt-2 text-center" style="background: greenyellow;  border-radius: 40px; padding: 1px;"><?php echo $gallery_item->heading; ?></h2>
  
              <h3 class="card-title mt-2 text-center"><?php echo $gallery_item->subheading; ?></h3>
            <h5 class="card-subtitle text-center"><span>&#8377;</span> <?php echo $gallery_item->cost; ?> per couple
            </h5>

            <?php endif; ?>
            
                           <?php if(strlen( $gallery_item->day1 )!=0): ?>

                <div class="card-text mt-3 ">

                    <?php echo $gallery_item->day1; ?>  
                </div> 
                <?php endif; ?>

                <?php if(strlen( $gallery_item->day2 )!=0): ?>

                <div class="card-text mt-3  ">

                    <?php echo $gallery_item->day2; ?>  
                </div> 
                <?php endif; ?>


                <?php if(strlen( $gallery_item->day3 )!=0): ?>

                <div class="card-text mt-3  ">

                    <?php echo $gallery_item->day3; ?>  
                </div> 
                <?php endif; ?>

                <?php if(strlen( $gallery_item->day4 )!=0): ?>

                <div class="card-text mt-3  ">

                    <?php echo $gallery_item->day4; ?>  
                </div> 
                <?php endif; ?>


                <?php if(strlen( $gallery_item->day5 )!=0): ?>

                <div class="card-text mt-3  " >

                    <?php echo $gallery_item->day5; ?>  
                </div> 
                <?php endif; ?>

   

 


        </div>
    </div>


    <?php if($index%3==2 || sizeof($galleryPackage)-1==$index): ?>
    </div>
   
  
    <?php endif; ?>

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


<?php $__env->stopSection(); ?> 

<div id="package" >
    <h2 class= "text-center"> <span class="font_package_heading"> Packages</span></h2><br>
    <br> 
    <div class="container">
       



            <?php echo $__env->yieldContent('image_pkg'); ?>  


      




    </div>

    <br> 
    <br> 
    <br> 





</div>


 
<?php $__env->startSection('contact'); ?>

<form    method="POST" id="enquiryform" action="/contact" style="padding-left: 15px;padding-right: 15px">

    <div class="row">
        <div class="col-sm-12 form-group">
            <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-6 form-group">
            <input class="form-control" id="mobile" name="mobile" placeholder="mobile" type="number" required>
        </div>
        <div class="col-sm-6 form-group">
            <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
        </div>
    </div>
    
         
    <textarea class="form-control" id="comments" name="comments"  placeholder="Comment" rows="5"></textarea><br>
  
    <div class="row">
        <div class="col-sm-12 form-group text-center">
  
            <button class="btn btn-default     centered button_round   " type="submit">Send</button>
       
        </div>
    </div>
   
</form>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('address'); ?>
<!-- Container (Pricing Section) -->
<div class="panel panel-default text-center">
    <div class="panel-heading">
        <h1 class="font_contact"> Address </h1>
    </div>

    <div class="panel-body">
        <br> <br>   <br> <br> 

        <ul style="list-style: none;"  >

            <li>    Address 1   </li>
            <li>   Address 2   </li>
            <li>    Address 3</li>
            <li>   ph:+91-345345345, +91-546456465  </li>
            <li>    Email:- a@a.com.com   </li>

        </ul>
 <br> <br>   <br> <br>




        <br>
        <br>

    </div>


</div> 
 <br> <br>   

<?php $__env->stopSection(); ?>

<div id="contact" >
    
 
 
    <!-- Container (Contact Section) -->
    <div class="container-fluid well well-sm ">
        <div class="row ">
            <br>
            
        </div>


        <div class="row">
            <div class="col-md-1">

            </div> 
            <div class="col-lg-4 "  >
                <div class="row ">
                    <br><br>
                </div>
                 <?php echo $__env->yieldContent('address'); ?>
            </div>

            <div class="col-lg-6 rounded">
                <br> 
                <br>
                <h2 class="text-center">CONTACT</h2> 
                <br>
                <br>

                <?php echo $__env->yieldContent('contact'); ?> 
              <div class="text-center">      <span id="contactmsg" class="company_moto_font " style="color: whitesmoke;background-color:maroon;font-size: 15px"></span>
                  
              </div>

            </div>

            <div class="col-sm-1">

            </div> 
        </div>

        <div class="row">










        </div>

        <div class="row">
            <br>
            <br>

        </div>

    </div>
    
          
</div>


<!-- Add Google Maps -->
 
        
  

<div class="map-responsive">
    <div id="map" style="height:400px;width:100%;" ></div>    
</div>
    <footer id="myFooter">
        <div class="container ">
            <div class="row">
                <div class="col-md-4 ">
                    <h5>Current Page</h5>
                    <ul>
                        <li><a href="#topnavid" >Page Top</a></li>
                        <li><a href="#wayanad">Wayanad</a></li>
                        <li><a href="#profile">Our Resort</a></li>
                        <li><a href="#contact">Contact</a></li>
                        <li><a href="#package">Packages</a></li>
                    </ul>
                </div>
                  
                 
                <div class="col-md-4">
                    <h5>Info</h5>
                    <ul>
                        
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Faq</a></li>
                        <li><a href="#">Login</a></li>
                         <li><a href="#">Register</a></li>

                    </ul>
                </div>
                
                 
                
               
                 
                 
                
                <div class="col-md-4">
                    <h5>Address</h5>
                    <ul>
                       <li> <a href="#">  Adress 1 </a> </li>
                        <li> <a href="#">  Adress 1 </a> </li>
                        <li> <a href="#">  Adress 1 </a> </li>
                        <li> <a href="#">  Adress 1</a> </li>
                        
                    </ul>
                </div>
            </div>
            <!-- Here we use the Google Embed API to show Google Maps. -->
            <!-- In order for this to work in your project you will need to generate a unique API key.  -->
 
        </div>
        <div class="social-networks">
            <a href="https://www.facebook.com" class="twitter"><i class="fa fa-twitter"></i></a>
            <a href="https://www.facebook.com" class="facebook"><i class="fa fa-facebook"></i></a>
            <a href="https://www.facebook.com" class="google"><i class="fa fa-google-plus"></i></a>
        </div>
        <div class="footer-copyright">
             
            <p>© 2017 Copyright Text </p>
        </div>
    </footer>