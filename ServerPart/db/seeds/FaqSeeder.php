<?php


use Phinx\Seed\AbstractSeed;

class FaqSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $this->execute("
            INSERT INTO tourDB1.Faq (id,Question,Answer) VALUES 
(1,'What is the home stay fee?','Banasura Hill Valley Home Stay charged a nominal resort fee to all guests in order to provide additional services that are often charged individually by other home stays.This fee covers services such as wireless internet,self parking.one bottle of water.')
,(2,'What can I do if someone gets sick before we arrive?','If a family member’s sickness inhibits you from your travel plans we will gladly rebook your getaway. However,should you decid to cancel,you will be refunded your deposit less 15% of your deposit so long as you cancel up to 7 days prior to your arrival date.If you cancel up to 4 days prior to arrival,then you will be refunded your deposit less 30% of your deposit. If your plans change at the last minute(or anytime within 3 days of arrival),call us and we’ll try to rebook your trip for different dates.You would only forfeit your deposit if we coudn’t rebook your getaway at all. You are subject to the best available rate on these new dates.')
,(3,'What kind of weather can we expect during our stay?','For the most up to date weather,please visit The Weather Channel.')
,(4,'Where can I find directions to the resort?','Check google map in the home page ')
,(5,'How secure is the web site when I’m booking online?','Our web site is secured by TLS and Go Daddy.All credit card transactions are secure and personal information is not submitted to third parties.')
,(6,'Do you accept prepaid cards and debit cards?','For prepaid cards,we recommend that you have at least 20% over your payment due in order for our systems to read the cards.Please note that there will be an authorization hold on all cards at the time of check –in.Debit cards are strongly discourages.Debit cards are strongly discouraged.The use of a debit card at the time of check in will initiate an immediate deduction from your bank account for the remaining balance of room and tax for your stay. ')
,(7,'How clean is the home stay?','Our home stay consistently rank high in cleanliness as measured by you,our guests! Our housekeeping staff works hard to make sure each room is cleaned to the highest standards and to keep our common areas spotless.We use green cleaning chemicals that also meet disinfection standards.')
,(8,'What’s in it for me?','You get the  best available deals,wayanad group tour,trekking,holiday packages and much more.')
,(9,'How do you get assistance?','We are avilable 24x7 on 8129723182 or 9947177040')
,(10,'How do I contact Banasura Hill Valley Home Stay?','Contact us (you can get the details from home page) at Banasura Hill Valley Home Stay, MN and representative will respond to you shortly.')
;

         ");
        
        $this->execute("
            INSERT INTO tourDB1.Faq (id,Question,Answer) VALUES 
(11,'What do I need to provide when I arrive at the lodge?','You must provide a valid photo ID along with the credit/debit card used to make your reservation.The remaining balence of room rates,packages,taxes and fees will be collected at the time of check-in.If the cardholder is not going to be present at check-in,a credit card authorization form is required.')
,(12,'What other area attractions are around the resort?','Banasura Sagar Earth Dam,meenmutty water falls,Banasura Hill .')
,(13,'What is the cancellation policy?','When bad weather or a famil member’s sickness inhibits you from your travel plans we will gladly rebbook your gateway.If you need to cancel your reservation, you will be refunded your deposit less 15% of your deposit so long as you cancel up to 7 days prior to your arrival date.If you cancel up to 4 days prior to arrival,then you will be refunded your deposit less 30% of your deposit.If your plans change at the last minute(or anytime within 3 days of arrival), call us and we’ll try to rebook your trip for different dates.You would only forfeit your deposit if we couldn’t rebook your your getaway at all.You are subject to the best available rate on these new dates.<br>*May not apply to group reservation.')
;
         ");
        
    }
}
