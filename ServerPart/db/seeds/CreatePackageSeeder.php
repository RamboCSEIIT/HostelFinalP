<?php

use Phinx\Seed\AbstractSeed;

class CreatePackageSeeder extends AbstractSeed
{
/**
 * Run Method.
 *
 * Write your database seeder using this method.
 *
 * More information on writing seeders is available here:
 * http://docs.phinx.org/en/latest/seeding.html
 */
public function run()
{
$faker = Faker\Factory::create();
$data = [];

                                   




$heading = [];
$subheading = [];

$cost=[];
$tentative=[];
$make_front=[];
$day1=[];
$day2=[];
$day3=[];
$day4=[];
$day5=[];



$heading[0]="1 Night 2 Days ";
$subheading[0]="Wayanad To Wayanad Tour";
//$image_link = [];
$make_front[0]=1;
$cost[0]=1300;
$tentative[0]=0;
$day1[0]="<b>Day One:</b> Sightseeing Vitrify View Point,Pookode lake, Chain tree,Banasura Earth Dam,Meenmutty Waterfalls,Karlad Lake and Overnight stay at hotel. ";
$day2[0]="<b>Day Two:</b> Muthanga Wildlife sanctuary, Edakkal Caves, Neelimala View Point, Heritage museum, Ambalavayal farm, Pandum Rock, Karapuzha Dam and drops at Calicut railway station";
$day3[0]=NULL;
$day4[0]=NULL;
$day5[0]=NULL;


$heading[1]="2 Nights  3 days ";
$subheading[1]="Calicut To Wayanad Tour";
//$image_link = [];
$make_front[1]=1;
$cost[1]=1600;
$tentative[1]=0;
$day1[1]="<b>Day One: </b> Pickup from calicut railway station at 7.30 am. Vythiri View Point, pookode lake, chain tree, Banasura Earth Dam, Meenmutty Waterfalls, Karlad Lake. Ovvernight stay at hotel";
$day2[1]="<b>Day Two: </b> Muthanga Wildlife sanctuary, Edakkal Caves, Neelimala View Point, Heritage museum, Ambalavayal farm, Pandum Rock, Karapuzha Dam and drops at Calicut railway station.";
$day3[1]=NULL;
$day4[1]=NULL;
$day5[1]=NULL;



$heading[2]="Ayurvedic Treatment";
$subheading[2]="";
//$image_link = [];
$make_front[2]=1;
$cost[2]=0;
$tentative[2]=1;
$day1[2]="<b>Rejuvenative Treatments:</b> Rejuvenative treatment are designed to revitalize the body tissue improve circulation and remove accumulated stress and toxins from mind and body.<br> <b>Curative Treatments:</b> Ateam of dedicated Ayurvedic Physicians meet the health needs of all our guests each of whom fills on a detaild health questionnaire and undergoes a basic medical examination.All the treatments may be selected only after the consultation of our physician ";
$day2[2]=NULL;
$day3[2]=NULL;
$day4[2]=NULL;
$day5[2]=NULL;


$heading[3]="1 Night  2 days ";
$subheading[3]="Mysore To Wayanad ";
//$image_link = [];
$make_front[3]=1;
$cost[3]=2300;
$tentative[3]=0;
$day1[3]=" <b>Day One: </b> Starts on 11.30 pm at Mysore Bus Stand. Vythiri View Point, pookode lake, chain tree, Banasura Earth Dam, Meenmutty Waterfalls, Karlad Lake. Overnight stay at hotel.";
$day2[3]="<b>Day Two:  </b>Muthanga Wildlife sanctuary, Edakkal Caves, Neelimala View Point, Heritage museum, Ambalavayal farm, Pandum Rock, Karapuzha Dam and drops at Mysore bus stand.";
$day3[3]=NULL;
$day4[3]=NULL;
$day5[3]=NULL;



$heading[4]="1 Night  2 days ";
$subheading[4]="Banglore To Wayanad ";
//$image_link = [];
$make_front[4]=1;
$cost[4]=2400;
$tentative[4]=0;
$day1[4]="<b>Day One : </b>Start on 11.30 pm at Banglore Bus Stand. SightseeingVythiri View Point, pookode lake, chain tree, Banasura Earth Dam, Meenmutty Waterfalls, Karlad Lake. Overnight stay at hotel.";
$day2[4]="<b>Day Two : </b>Muthanga Wildlife sanctuary, Edakkal Caves, Neelimala View Point, Heritage museum, Ambalavayal farm, Pandum Rock, Karapuzha Dam And drops at Banglore bus stand.";
$day3[4]=NULL;
$day4[4]=NULL;
$day5[4]=NULL;
 
$heading[5]="1 Night  2 days ";
$subheading[5]="Chennai To Wayanad Tour ";
//$image_link = [];
$make_front[5]=1;
$cost[5]=1650;
$tentative[5]=0;
$day1[5]="<b>Day One: </b>Pickup from calicut railway station at 7.30 am. Vythiri View Point, pookode lake, chain tree, Banasura Earth Dam, Meenmutty Waterfalls, Karlad Lake. Overnight stay at hotel.";
$day2[5]="<b>Day Two: </b>Muthanga Wildlife sanctuary, Edakkal Caves, Neelimala View Point, Heritage museum, Ambalavayal farm, Pandum Rock, Karapuzha Dam and drops at Calicut railway station";
$day3[5]=NULL;
$day4[5]=NULL;
$day5[5]=NULL;


 



$heading[6]="2 Nights 3 Days ";
$subheading[6]="Wayanad To Wayanad";
//$image_link = [];
$make_front[6]=1;
$cost[6]=2550;
$tentative[6]=0;
$day1[6]="<b>Day One: </b>Starting at 08.30 Am at Kalpetta. Early morning fresh up and going to Sightseeing Vythiri View Point, pookode lake, chain tree, Banasura Earth Dam, Meenmutty Waterfalls, Karlad Lake. Overnight stay at hotel.";
$day2[6]="<b>Day Two: </b>Muthanga Wildlife sanctuary, Edakkal Caves, Neelimala View Point, Heritage museum, Ambalavayal farm, Pandum Rock, Karapuzha Dam Sulthan Bathery Jain Temple.";
$day3[6]="<b>Day Three: </b>Starts at 7.30 amThirunelly Temple, Tholpetty wild life sanctuary. KuruvaIsland, Seetha Lava Kushatemple";
$day4[6]=NULL;
$day5[6]=NULL;

 
$heading[7]="1 Night 2 Days  ";
$subheading[7]="Wayanad To Wayanad Family Packages";
//$image_link = [];
$make_front[7]=1;
$cost[7]=5700;
$tentative[7]=0;
$day1[7]="<b>Day One: </b>First day tour Starting at 08.30 Am at Kalpetta. Vythiri View Point, pookode lake, chain tree, Banasura Earth Dam, Meenmutty Waterfalls, Karlad Lake. Overnight stay at hotel.";
$day2[7]="<b>Day Two: </b>Second day tour with Breakfast Start 6.30 AM at Muthanga Wildlife sanctuary, Edakkal Caves, Neelimala View Point, Heritage museum, Ambalavayal farm, Pandum Rock, Karapuzha Dam Sulthan Bathery Jain Temple.";
$day3[7]=NULL;
$day4[7]=NULL;
$day5[7]=NULL;


 /* $heading[1] = "Two Night  Three days<br>Calicut To Wayanad Tour ";
$desc[1] = "  <br><br><b>Day One <br><br></b>

Pickup from calicut railway station at 7.30 am.Vythiri View Point,pookode lake,chain tree,Banasura Earth Dam,Meenmutty Waterfalls,Karlad Lake. Ovvernight stay at hotel
<br><br><b>Day Two<br><br></b>
 Muthanga Wildlife sanctuary, Edakkal Caves,Neelimala View Point,Heritage museum,Ambalavayal farm,Pandum Rock,Karapuzha Dam and drops at Calicut railway station.
";

$cost[1]=1600;
$tentative[1]=0;

////////////////////////////////////////////////////////////////

$heading[2] = "Wayanad  Ayurvedic Treatment ";
$desc[2] = "<b><br><br>Rejuvenative Treatments:<br><br></b> Rejuvenative treatment are designed to revitalize the body tissue improve circulation and remove accumulated stress and toxins from mind and body.  <b> <br><br>Curative Treatments:<br><br></b>  Ateam of dedicated Ayurvedic Physicians meet the health needs of all our guests each of whom fills on a detaild health questionnaire and undergoes a basic medical examination.All the treatments may be selected only after the consultation of our physician  ";
$cost[2]=10000;
$tentative[2]=1;
 <b><br><br>Rejuvenative Treatments:<br><br></b> Rejuvenative treatment are designed to revitalize the body tissue improve circulation and remove accumulated stress and toxins from mind and body.  <b> <br><br>Curative Treatments:<br><br></b>  Ateam of dedicated Ayurvedic Physicians meet the health needs of all our guests each of whom fills on a detaild health questionnaire and undergoes a basic medical examination.All the treatments may be selected only after the consultation of our physician 

///////////////////////////////////////////////////////////////////////////////////




$heading[3] = "One Night  Two days <br>Mysore To Wayanad ";
$desc[3] = "
<br><br><b>Day One <br><br></b>


Starts on 11.30 pm at Mysore Bus Stand.Vythiri View Point,pookode lake,chain tree,Banasura Earth Dam,Meenmutty Waterfalls,Karlad Lake. Overnight stay at hotel.
<br><br><b>Day Two<br><br></b>
Muthanga Wildlife sanctuary, Edakkal Caves,Neelimala View Point,Heritage museum,Ambalavayal farm,Pandum Rock,Karapuzha Dam and drops at Mysore  bus stand.

 ";
$cost[3]=2300;
$tentative[3]=0;


/////////////////////////////////////////////////////

$heading[4] = "One Night  Two days <br>Banglore To Wayanad";
$desc[4] =  "
<br><br><b>Day One <br><br></b>


Start on 11.30 pm at Banglore Bus Stand.Sightseeing
Vythiri View Point,pookode lake,chain tree,Banasura Earth Dam,Meenmutty Waterfalls,Karlad Lake.Overnight stay at hotel.


<br><br><b>Day Two<br><br></b>
Muthanga Wildlife sanctuary, Edakkal Caves,Neelimala View Point,Heritage museum,Ambalavayal farm,Pandum Rock,Karapuzha Dam And drops at Banglore bus stand.

";
$cost[4]=2400;
$tentative[4]=0;

//////////////////////////////////////////////////////////////////////////////////////


$heading[5] = "One Night  Two days<br>Chennai To Wayanad Tour";
$desc[5] = "
<br><br><b>Day One <br><br></b>


Pickup from calicut railway station at 7.30 am .Vythiri View Point,pookode lake,chain tree,Banasura Earth Dam,Meenmutty Waterfalls,Karlad Lake.Overnight stay at hotel.


<br><br><b>Day Two<br><br></b>
Muthanga Wildlife sanctuary, Edakkal Caves,Neelimala View Point,Heritage museum,Ambalavayal farm,Pandum Rock,Karapuzha Dam and drops at Calicut railway station

 ";
$cost[5]=1650;
$tentative[5]=0;

////////////////////////////////////////////
$heading[6] = "Two Night Three Days<br>Wayanad To Wayanad";
$desc[6] = "
<br><br><b>Day One <br><br></b>


Starting at 08.30 Am at Kalpetta.Early morning fresh up and going to Sightseeing Vythiri View Point,pookode lake,chain tree,Banasura Earth Dam,Meenmutty Waterfalls,Karlad Lake. Overnight stay at hotel.


<br><br><b>Day Two<br><br></b>
Muthanga Wildlife sanctuary, Edakkal Caves,Neelimala View Point,Heritage museum,Ambalavayal farm,Pandum Rock,Karapuzha Dam Sulthan Bathery Jain Temple.

<br><br><b>Day Three<br><br></b>
Starts at 7.30 amThirunelly Temple,Tholpetty wild life sanctuary.KuruvaIsland,Seetha Lava Kushatemple

 ";
$cost[6]=2550;
$tentative[6]=0;

///////////////////////////////////////


$heading[7] = "One Night Two Days Family Packages<br>Wayanad To Wayanad";
$desc[7] = "
<br><br><b>Day One <br><br></b>


First day tour Starting at 08.30 Am at Kalpetta. Vythiri View Point,pookode lake,chain tree,Banasura Earth Dam,Meenmutty Waterfalls,Karlad Lake. Overnight stay at hotel.


<br><br><b>Day Two<br><br></b>
Second day tour with Breakfast Start 6.30 AM at Muthanga Wildlife sanctuary, Edakkal Caves,Neelimala View Point,Heritage museum,Ambalavayal farm,Pandum Rock,Karapuzha Dam Sulthan Bathery Jain Temple.

 

 
 ";
$cost[7]=5700;
$tentative[7]=0;
*/

/////////////////////////////////////






         
         for ($i = 0; $i < 8; $i++) 
         {
             
             
            
            
               
            $data[] = 
            [
                            'heading'=>$heading[$i],
                            'subheading'=>$subheading[$i],
                            'image_link' =>"02_IMAGES/aa_Home_Page/02_Packages/package_".$i.".jpg",            
                            //$image_link = [];
                            'make_front'=>$make_front[$i],
                            'cost'=>$cost[$i],
                            'tentative'=>$tentative[$i],
                            'day1'=>$day1[$i],
                            'day2'=>$day2[$i],
                            'day3'=>$day3[$i],
                            'day4'=>$day4[$i],
                            'day5'=>$day5[$i]
                   ];

        }

         $this->insert('galleryPackage', $data);
    }
}
