<?php namespace  tour\Controllers;


use tour\Validation\Validator;
use duncan3dc\Laravel\BladeInstance;
use Illuminate\Database\Capsule\Manager as DB;
use tour\auth\LoggedIn;
class AuthenticationController extends BaseController
{

    protected $signer='';
   
    
    public function getShowLoginPage( ) 
    {
       
           echo $this->blade->render('login_page', [
           
            'page_name' => '#login-page',
               'signer' => $this->signer
        ]);           
        
    }



    public function postShowLoginPage()
    {
    /*
        if (!$this->signer->validateSignature($_POST['_token']))
        {
           $_SESSION['msg'] = ["Insecure login!"];
            echo $this->blade->render("login_page", [
                'signer' => $this->signer,
                'page_name' => '#login-page'
            ]); 
               unset( $_SESSION['msg']);
            exit();
        }*/
         
        
         echo "posted";
         $message = '';
         
         unset($_SESSION['msg']);
         
          $okay = true;
          $email = $_REQUEST['email_name'];
          $password = $_REQUEST['password_name'];
          
          
          $user =  DB::select('SELECT * FROM users WHERE email = :email',
                                                        array(
                                                                 'email'  => $email
                                                             )
                                                  );
          
          
         if ($user != null) 
        {
            // validate credentials
            if ( !password_verify($password, $user[0]->password)) 
            {
                $okay = false;
                $message= $message."<br>User exists but password miss match";
            }
            else
            {
                
                    if ($user[0]->active==0) 
                    {

                            $okay = false;
                            $message= $message."<br> You have not verified your registration<br> Check your mail inbox for registration verification";

                    }

            }
        } 
        else 
        {
            $okay = false;
            $message= $message."<br>No user exists with".$email;
        }
        
        
     
        
        
        
        if ($okay) 
        {
            $_SESSION['user'] = $user;
            header("Location: /");
            exit();
        } 
        else 
        {
            
           
            $_SESSION['msg'] = ["Invalid login!".$message];
            echo $this->blade->render("login_page", [
                'signer' => $this->signer,
                'page_name' => '#login-page'
            ]);
            unset($_SESSION['msg']);
            exit(); 
        }


         
 

    }


    public function getLogout()
    {
        unset($_SESSION['user']);
        session_destroy();
        header("Location: /login");
        exit();
    }
    
    public function getTestUser()
    {

        dd(LoggedIn::user());
    }

}
?>