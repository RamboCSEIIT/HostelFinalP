var gulp                                         = require('gulp');
var uglify                                = require('gulp-uglify');
var concat                                = require('gulp-concat');
var cleanCSS                           = require('gulp-clean-css'); 
var autoprefixer                    = require('gulp-autoprefixer');
var plumber                              = require('gulp-plumber');
var sourcemaps                        = require('gulp-sourcemaps');
var sass                                    = require('gulp-sass');
var del                                           = require('del');

// Image compression
var imagemin                            = require('gulp-imagemin');
var imageminPngquant                    = require('gulp-pngquant');
var imageminJpegRecompress   = require('imagemin-jpeg-recompress');


var cssbeautify                      = require('gulp-cssbeautify');
var jsbeautify                       =  require('gulp-jsbeautify'); 

var browserSync                 = require('browser-sync').create();


 

 
 



//////////////////////////////////////////////////////////////////////////

//CSS Path // File paths -- ** take folders
var CSS_IN_PATH  = '00_CSS/**/*.css';
var CSS_OUT_PATH = 'public/00_CSS';

// Styles
gulp.task('styles', function () 
{
	console.log('starting styles task');
	return gulp.src(['00_CSS/reset.css', CSS_IN_PATH])
		.pipe(plumber(function (err)
                {
			console.log('Styles Task Error');
			console.log(err); 
                        // close rest but keep gulp
			this.emit('end');
		}))   
                .pipe(sourcemaps.init())
                .pipe(autoprefixer())
		.pipe(concat('style.css'))
                .pipe(cleanCSS({compatibility: 'ie8'}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(CSS_OUT_PATH));
 
        console.log('finished styling');
		 
});

////////////////////////////////////////////////////////////////////////////////////////

//Script Path
var SCRIPTS_IN_PATH  = '01_SCRIPTS/**/*.js';
var SCRIPTS_OUT_PATH = 'public/01_SCRIPTS';


// Scripts
gulp.task('scripts', function () 
{
	console.log('starting scripts task');

	return gulp.src(SCRIPTS_IN_PATH)
		.pipe(plumber(function (err)
                {
			console.log('Scripts Task Error');
			console.log(err);
			this.emit('end');
		}))
		//.pipe(sourcemaps.init())
                .pipe(concat('scripts.js'))  
 //               .pipe(uglify())
		//.pipe(sourcemaps.write()) 
		.pipe(gulp.dest(SCRIPTS_OUT_PATH))
                .pipe(browserSync.reload({stream: true}));     
        
        
             
                
});

//////////////////////////////////////////////////////////////////////////
var IMAGES_IN_PATH  = '02_IMAGES/**/*.{png,jpeg,jpg,svg,gif}';
var IMAGES_OUT_PATH = 'public/02_IMAGES';


 

gulp.task('images', function () {
  return gulp.src(IMAGES_IN_PATH)
    .pipe(imagemin([
      imagemin.gifsicle(),
      imageminJpegRecompress({
        loops:4,
        min: 50,
        max: 50,
        quality:'high' 
      }),
      imagemin.optipng(),
      imagemin.svgo()
    ])).pipe(gulp.dest(IMAGES_OUT_PATH));
 
});



 
 
////////////////////////////////////////////////////////////////////////////



gulp.task('browser-sync', ['sass'], function()
{
    browserSync.init({
    
    proxy: {
        target: "localhost:80"//, // can be [virtual host, sub-directory, localhost with port]
    //    ws: true // enables websockets
    }
    });
});
 

gulp.task('browser-reload', ['browser-sync'], function () 
{
    gulp.watch(SASS_IN_PATH, ['sass']);   
    
});



var SASS_IN_PATH  = '00_SASS/**/*.scss';
var SASS_OUT_PATH = 'public/00_CSS';

 

gulp.task('sass', function () 
{
	console.log('!!!!starting Sass task!!!');
	return gulp.src(SASS_IN_PATH)
		.pipe(plumber(function (err) 
                {
			console.log('Styles Task Error');
			console.log(err);
                        // close rest but keep gulp
			this.emit('end');
		}))   
                .pipe(sourcemaps.init())
                .pipe(autoprefixer())
//		.pipe(concat('styles.css'))
//                .pipe(cleanCSS({compatibility: 'ie8'}))
//Sass does this minification and concantenation
 
		.pipe(sass({
			outputStyle: 'compressed'
		})) 
 //               .pipe(sass())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(SASS_OUT_PATH))
                .pipe(browserSync.reload({stream: true}));       
        
                 console.log('!!!!Sass task finished!!!');
        
		 
});


 

 
//////////////////////////////////////////////////////////////////////////

var SASS_IN_STYLE_PATH  = 'public/00_CSS/**/*.css';
var SASS_OUT_STYLE_PATH = 'public/00_CSS/';

 

gulp.task('css-format', function () 
{
     return gulp.src(SASS_IN_STYLE_PATH)
        .pipe(cssbeautify({
            indent: '  ',
            openbrace: 'separate-line',
            autosemicolon: true
        }))
        .pipe(gulp.dest(SASS_OUT_STYLE_PATH));
   
    
    
});

var JS_IN_STYLE_PATH  = 'public/01_SCRIPTS/**/*.js';
var JS_OUT_STYLE_PATH = 'public/01_SCRIPTS/';


gulp.task('js-format', function () 
{
     return gulp.src(JS_IN_STYLE_PATH)
        .pipe(jsbeautify({
            indent: '  ',
            openbrace: 'separate-line',
            autosemicolon: true
        }))
        .pipe(gulp.dest(JS_OUT_STYLE_PATH));
 
    
    
});


//////////////////////////////////////////////////////////////////////////

var SASS_IN_STYLE_PATH  = 'public/00_CSS/**/*.css';
var SASS_OUT_STYLE_PATH = 'public/00_CSS/';

 

 


 



gulp.task('watch',['default'], function () 
{
	console.log('Starting watch task');
 	 
         
       
         
        
        //only scripts task needed to run (file path,which script)
	gulp.watch(SCRIPTS_IN_PATH, ['scripts']); 
       // gulp.watch(CSS_IN_PATH, ['styles']);
        gulp.watch(SASS_IN_PATH, ['sass']);
        
        
        
    
   
});


 

/*
gulp.task('default', ['clean','images',  'sass', 'scripts'], function () {
	console.log('Starting default task');
});*/


gulp.task('default', ['clean','browser-sync', 'sass', 'scripts'], function () 
{
	console.log('Starting default task');
});

/*
gulp.task('clean', function () {
	return del.sync([
		 SCRIPTS_OUT_PATH,IMAGES_OUT_PATH,SASS_OUT_PATH
	]);
});*/

gulp.task('clean', function () {
	return del.sync([
		 SCRIPTS_OUT_PATH,SASS_OUT_PATH
	]);
});

 

