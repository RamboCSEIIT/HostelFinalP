<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Gulp is an exciting front-end web development workflow system, mix it with NPM - a package manager for Node.js and front-end libraries - and you have a toolbelt to be reckoned with.">
    <meta name="author" content="George Webb">
    <link rel="icon" type="image/png" href="/favicon.png">

    <title>Up and running: Gulp and NPM for front-end web development - George Webb</title>

    <link rel="stylesheet" href="/dist/css/style.css?cb=1487676573">

    <script>
        document.documentElement.className = "";
    </script>

    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div class="page-header">
        <h2>
            <a href="/">George Webb</a>
            <a href="#" class="pull-right toggle-menu">
                <span class="glyphicon glyphicon-menu-hamburger"></span>
                <span class="sr-only">Menu</span>
            </a>
        </h2>

        <div class="page-menu">
            <div class="page-menu-inner">
                <div class="page-menu-item">
                    <a href="/">Blog</a>
                </div>

                <div class="page-menu-item">
                    <a href="/about">About</a>
                </div>

                <div class="page-menu-item">
                    <a href="/contact">Contact</a>
                </div>

                <div class="page-menu-item">
                    <a href="/feed">RSS</a>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="row">
<div class="col-sm-8 col-xs-12 content post" itemscope itemtype="https://schema.org/BlogPosting">
    <h1 itemprop="name">Up and running: Gulp and NPM for front-end web development</h1>

    <p>
        <span itemprop="dateCreated">
            Feb 21st 2017        </span> |
        <span itemprop="author">
            George Webb        </span>
    </p>

    <div class="post-content" itemprop="articleBody"><p>Gulp is one of the most exciting tools I've added to my front-end web development belt over the last couple of years. I
primarily use it to automate my front-end workflow and for building distributable assets, however the possible uses go
far beyond that. Add NPM into the mix, for package management, and baby you got a stew going.</p>
<p>Below, I'll describe a basic setup of using Gulp and NPM for creating a simple bootstrap web app. If you want to skip
straight to the good stuff, I have a 'starter project' template at my GitHub:
<a href="https://github.com/gaw508/bootstrap-starter-project">https://github.com/gaw508/bootstrap-starter-project</a></p>
<h3>NPM</h3>
<p>If you're not familiar with NPM - Node Package Manager - it's the package manager for the Node.js language, i.e. a
tool for managing JavaScript libraries and packages (usually from the NPM registry). Despite being primarily for the
Node.js language, it also contains many packages for front-end web development, including jQuery, Bootstrap and many
others.</p>
<p>The NPM configuration in your project is defined within a <code>package.json</code> file. This file can contain numerous configuration
options, including - most importantly - which packages are required for the project. Also, if you wanted to contribute a
package to the NPM registry, this file would be used to put information about your package.</p>
<p>You'll need both Node.js and NPM installed to use Gulp and for the setup described below. To install Node.js and NPM,
visit <a href="https://nodejs.org">https://nodejs.org</a></p>
<h3>Gulp</h3>
<p>Gulp is a fantastic little tool for automating your workflow - done by writing simple JavaScript to manipulate streams
of files. While Gulp can be used for an enormous number of tasks, my use of it is generally restricted to front-end web
development workflow.</p>
<p>Two important concepts in Gulp are tasks and watchers. Tasks are units of code which can be executed, either on the command
line (using the gulp CLI tool) or from within other tasks and watchers. Watchers 'watch' for changes to specific files and
directories, and trigger a callback - for example running a task.</p>
<p>Within your project, Gulp code is contained within the <code>gulpfile.js</code> file. You can include other JS files and packages in
your gulpfile, but for simple projects you will likely define all of your tasks and watchers here.</p>
<p>Gulp can be installed globally by running:</p>
<pre><code>npm install gulp-cli -g
npm install gulp -D
gulp —help</code></pre>
<h3>Setting up the project</h3>
<p>The project we're going to create is a simple one-page web app, using Bootstrap and jQuery (exciting stuff, eh?). All of
our JS and CSS assets will be concatenated, minified, and copied into our public html directory, along with our static
assets, i.e. images.</p>
<p>The project's file structure will be:</p>
<pre><code>project_root/
    src/                        # Source assets
        css/                    # Source CSS files
            blah_styles.css
        js/                     # Source JS files
            blah_script.js
        images/                 # Source image files
            pretty.png
    web/                        # The web root/public HTML
        dist/                   # Distributable assets - populated by gulp
        index.html              # App HTML file
    .gitignore
    gulpfile.js                 # Gulpfile for project
    package.json                # NPM configuration</code></pre>
<p>In the following sections we'll work through the contents of the files.</p>
<h3>Requiring our packages with NPM</h3>
<p>The first thing we'll do is create our <code>package.json</code> file. To start with this can just be an empty JSON object:</p>
<pre><code>{

}</code></pre>
<p>Now we need to install the packages for our project:</p>
<ul>
<li>Gulp: The Gulp tool.</li>
<li>jQuery: JavaScript library for our web app.</li>
<li>Bootstrap: HTML/CSS/JS framework for our web app</li>
<li>Gulp Concat: A package for concatenating files in Gulp</li>
<li>Gulp Minify CSS: A package for minifying CSS files in Gulp</li>
<li>Gulp Uglify: A package for minifying JS files in Gulp</li>
<li>Live Server: Simple HTTP Server for testing our web app</li>
</ul>
<p>It's one simple command to install them all:</p>
<pre><code>npm install --save bootstrap jquery gulp gulp-concat gulp-minify-css gulp-uglify live-server</code></pre>
<p>You'll notice two things: first, the contents of <code>package.json</code> has been updated with the names of all of the packages;
second, all of the packages are installed into a new directory called <code>node_modules</code>. We don't want to include the
files in <code>node_modules</code> in our version control, only the references to them in <code>package.json</code>. For git, we can add this
directory to our project's <code>.gitignore</code> file.</p>
<p><code>.gitignore</code></p>
<pre><code>node_modules</code></pre>
<h3>Writing our gulpfile</h3>
<p>Now we have all of our required packages, we can write our build script in <code>gulpfile.js</code>. The first thing to do is require
all of the gulp tools we want to use:</p>
<pre><code>var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    minifyCSS = require('gulp-minify-css');</code></pre>
<p>Next, we will write our CSS task:</p>
<pre><code>gulp.task('css', function() {
    gulp.src([
            'node_modules/bootstrap/dist/css/bootstrap.css',
            'src/css/**/*.css'
        ])
        .pipe(minifyCSS())
        .pipe(concat('style.css'))
        .pipe(gulp.dest('web/dist/css'));
});</code></pre>
<p><code>gulp.task()</code> is the function used to define a task, accepting two arguments, the task name and the function to call when the
task is run.</p>
<p><code>gulp.src()</code> creates a stream of files, which can be piped into different tools. You'll notice that the bootstrap css file is
added to the stream, along with any CSS file within the source CSS directory. The file stream is first piped into <code>minifyCSS()</code>,
which minifies each of the CSS files individually. Then the stream is piped into <code>concat()</code>, which concatenates all of the
files into a single file, called <code>style.css</code>. The final pipe is into <code>gulp.dest()</code>, which outputs our concatenated file into
the <code>web/dist/css</code> directory. Simple stuff!</p>
<p>The next task to write is the JS task, which is very similar to the CSS task:</p>
<pre><code>gulp.task('js', function() {
    gulp.src([
            'node_modules/jquery/dist/jquery.js',
            'node_modules/bootstrap/dist/js/bootstrap.js',
            'src/js/**/*.js'
        ])
        .pipe(uglify())
        .pipe(concat('script.js'))
        .pipe(gulp.dest('web/dist/js'));
});</code></pre>
<p>Again, <code>gulp.src()</code> creates a file stream including jQuery, the Bootstrap JS file, as well as any JS file within the source
JS directory. This is then piped into uglify, the JS minifying tool, concatenated, and outputted to <code>web/dist/js/script.js</code>.</p>
<p>Next is the image task, which simply copies images from our source directory into our distributable directory. This task is
very simple for the purposes of this article, but could be expanded to resize images, watermark them or combine them into a
sprite.</p>
<pre><code>gulp.task('images', function() {
    gulp.src([
            'src/images/**/*'
        ])
        .pipe(gulp.dest('web/dist/images'));
});</code></pre>
<p>You'll notice the stream of image files is simply piped straight into the distributable images dir without any
modifications.</p>
<p>The final task is the default task. This is the task which is run if the Gulp CLI tool is run with no arguments, and will
likely be the task you run most often. For our project, the default task will simply run all of our above tasks, one by one.</p>
<pre><code>gulp.task('default', function() {
    gulp.run('js', 'css', 'images');
});</code></pre>
<p>Now we have defined all of our 'standard' tasks, we can write a task to start watching for file changes. We'll set it up
so that if there are any changes to CSS, JS or image files, the relevant task will be run automatically.</p>
<pre><code>gulp.task('watch', function() {
        gulp.run('default');

        gulp.watch('src/css/**/*.css', function(event) {
            console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
            gulp.run('css');
        });

        gulp.watch('src/js/**/*.js', function(event) {
            console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
            gulp.run('js');
        });

        gulp.watch('src/images/**/*', function(event) {
            console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
            gulp.run('images');
        });
});</code></pre>
<p>We've first created a task called 'watch', which, when run, will start all of our watchers. Next the default task is run,
to ensure any existing changes are built before starting watchers. After this, three watchers are defined:</p>
<ul>
<li>On source CSS files - calling the css task</li>
<li>On source JS files - calling the js task</li>
<li>On source image files - calling the images task</li>
</ul>
<p>Next we'll finish creating our simple HTML, CSS and JS files, before looking into how we can run our Gulp tasks.</p>
<h3>Filling in the blanks</h3>
<p>In order to make our web app, we need to (quickly) write an HTML file, a JS file and a CSS file, plus adding an image - this
is so we can see how our Gulp build process works.</p>
<p><code>src/css/blah_styles.css</code></p>
<pre><code>.my-container {
    background: #dedede;
}</code></pre>
<p><code>src/js/blah_script.js</code></p>
<pre><code>$(document).ready(function() {
    console.log('Ready!');
});</code></pre>
<p><code>src/images/yeah.png</code></p>
<pre><code>Any old image file you have knocking about...</code></pre>
<p><code>web/index.html</code></p>
<pre><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
&lt;head&gt;
    &lt;meta charset="utf-8"&gt;
    &lt;meta http-equiv="X-UA-Compatible" content="IE=edge"&gt;
    &lt;meta name="viewport" content="width=device-width, initial-scale=1"&gt;

    &lt;title&gt;NPM and Gulp Tutorial&lt;/title&gt;

    &lt;link href="dist/css/style.css" rel="stylesheet"&gt;
&lt;/head&gt;
&lt;body&gt;

    &lt;div class="container my-container"&gt;
        &lt;div class="page-header"&gt;
            &lt;h3&gt;NPM and Gulp Tutorial&lt;/h3&gt;
        &lt;/div&gt;

        &lt;div&gt;
            &lt;p&gt;Content&lt;/p&gt;
            &lt;img src="dist/images/yeah.png"&gt;
        &lt;/div&gt;
    &lt;/div&gt;

    &lt;script src="dist/js/script.js"&gt;&lt;/script&gt;
&lt;/body&gt;
&lt;/html&gt;</code></pre>
<p>Notice how in the HTML file we reference the output files of our build process, and not the source files.</p>
<h3>Running tasks</h3>
<p>We are now all set for building our distributable assets, by running gulp. Open up a command line window and go to
the root directory of the project. Then run:</p>
<pre><code>gulp</code></pre>
<p>This runs the default task in our <code>gulpfile.js</code> - which then runs our CSS, JS and images tasks. Once complete, you will
notice three new files:</p>
<ul>
<li><code>web/dist/css/style.css</code> - our concatenated and minified CSS file</li>
<li><code>web/dist/js/script.js</code> - our concatenated and minified JS file</li>
<li><code>web/dist/images/yeah.png</code> - our copied image file</li>
</ul>
<p>To preview our web app, we can use the live server package we installed earlier:</p>
<pre><code>live-server web/</code></pre>
<p>Voila! Your browser will open and the web app will appear.</p>
<p>If you wanted to run a single task, say the CSS task, you could run:</p>
<pre><code>gulp css</code></pre>
<p>This would only concatenate and minify CSS files, without touching JS and image files.</p>
<p>To start the watchers, run:</p>
<pre><code>gulp watch</code></pre>
<p>Now any time you make a change to any file under <code>src/js</code>, <code>src/css</code> or <code>src/images</code> they will be automatically built.</p>
<h3>Next steps</h3>
<p>Now that your front-end workflow is 10x more efficient you have no excuses, go and ship something cool - Go! Now!</p>
<p>You can find the tutorial code at <a href="https://github.com/gaw508/tutorials/tree/master/gulp-and-npm">https://github.com/gaw508/tutorials/tree/master/gulp-and-npm</a> and a 'starter project' template at
<a href="https://github.com/gaw508/bootstrap-starter-project">https://github.com/gaw508/bootstrap-starter-project</a></p></div>

    <div class="other-posts">
        <h4>Some Other Posts</h4>

        <ul>
                            <li>
                    <a href="/posts/distributed-php-logging-with-monolog-and-papertrail">
                        Distributed PHP Logging with Monolog and Papertrail                    </a>
                </li>
                            <li>
                    <a href="/posts/sending-php-email-with-mandrill-and-beanstalkd">
                        Sending Email from PHP with Mandrill and Beanstalkd                    </a>
                </li>
                            <li>
                    <a href="/posts/mailchimp-php-sdk-create-custom-sign-up-form">
                        Using the MailChimp PHP SDK to Create a Custom Sign-up Form                    </a>
                </li>
                    </ul>
    </div>

    <p class="post-footer">
        <a href="/">Back to posts</a>
    </p>
</div>

<div class="col-sm-4 col-xs-12 sidebar">
    <form id="subscribe-form" method="post" action="/newsletter/subscribe?form=true">
        <h4>Subscribe to get my best content.</h4>

        <div class="subscription-feedback">
                    </div>

        <div class="form-group">
            <div class="input-group">
                <label class="input-group-addon">First Name</label>
                <input class="form-control" name="first_name" placeholder="first name">
            </div>
        </div>

        <div class="form-group">
            <div class="input-group">
                <label class="input-group-addon">Email Address</label>
                <input class="form-control" name="email_address" placeholder="email address">
            </div>
        </div>

        <p>
            No spam, ever. You can unsubscribe at any time.
        </p>

        <div class="form-group">
            <button class="btn btn-success pull-right subscribe-submit" type="submit">Join the newsletter</button>
        </div>
    </form>
</div>

    </div>

    <div class="page-footer">
        &copy; George Webb 2017            <span class="pull-right">
                <a href="#" class="back-to-top">Back to top</a>
            </span>
    </div>
</div>

<link href='https://fonts.googleapis.com/css?family=Lato:400,700|Ubuntu' rel='stylesheet' type='text/css'>
<script src="/dist/js/script.js?cb=1487676573"></script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-47142365-3', 'auto');
        ga('send', 'pageview');

    </script>
</body>
</html>
