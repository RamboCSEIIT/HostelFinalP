<!--  It says document type for browser as html 5 file -->
<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
    <!--  For IE only -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  

    <meta name="description" content="">
    <meta name="author" content="">

  
  
  
 
  
    <link rel="shortcut icon" href="#" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="/assets/footer.css" rel="stylesheet">

 
 
    

    @yield('cssBlock')


  
       
        
        
        
        


  <title>    @yield('title')  </title>
</head>

<body>

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> 
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
     
    @if (tour\auth\LoggedIn::user() && tour\auth\LoggedIn::user()[0]->access_level == 2)
     <script src="//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.7.3/ckeditor.js"></script> 
     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
 
    @endif
  
    
    
    @include('topnav_page')
    @yield('carousel')
   

   <div class="container">
    
    
            <div class="row">
                 <div class="col-md-12">
                     <br>
                     <br>
                      @include('error_message_page')
                 </div>
             </div>

     @yield('content')
 
    </div>

 

      
    <script>
    $( document ).ready(function() 
    {
       // console.log( "document loaded" );
        //about-tour
       
       
       var id_of_nav = '{{ $page_name }}';
       var id_of_nav_hash = '#';
       var id_of_nav_newpage = '@';
       
       if(id_of_nav.localeCompare(id_of_nav_hash) && id_of_nav.localeCompare(id_of_nav_newpage) )
       {
            $(id_of_nav).addClass('active');
       }
        
       
       
        @yield('page_make')
    
       
       
       
       
        //console.log( deadline );
        
        
    });
    
 
     
  </script>
  

    
      
 @yield('bottomjs')
  
</body>

</html>
