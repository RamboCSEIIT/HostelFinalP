@extends('base_page')


   @section('title') 
      Enquiry
    @stop
   
     <!-- auto matically takes this as red --> 
@section('cssBlock') 
 
      
          
@stop
  

@section('content')

    <h1>Add Enquiry</h1>

    <form method="post" name="enquiryform"
        class="form-horizontal" id="testimonialform" action="/add-enquiry">

        <div class="form-group">
          <label for="title" class="col-sm-2 control-label">Title</label>
          <div class="col-sm-10">
            <input type="text" class="form-control required" id="title"
              name="title" placeholder="Title">
          </div>
        </div>

        <div class="form-group">
          <label for="enquiry" class="col-sm-2 control-label">Enquiry</label>
          <div class="col-sm-10">
            <textarea class="form-control required" name="enquiry"></textarea>
          </div>
        </div>

        <hr>

        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Add Enquiry</button>
          </div>
        </div>
    </form>

    <br>
    <br>
@stop

@section('bottomjs')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>

<script>
 $(document).ready(function(){
    $("#testimonialform").validate();
});
</script>
@stop
