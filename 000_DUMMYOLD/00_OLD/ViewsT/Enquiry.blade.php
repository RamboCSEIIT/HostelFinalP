@extends('base_page')
  
 @section('title') 
      Enquiry  
  @stop
  
  
 
  @section('cssBlock') 
    
  @stop
 

@section('content')
    <h1> </h1>

    <div class="list-group">
      <a href="#" class="list-group-item active">
        <h4 class="list-group-item-heading">Enquiries</h4>
      </a>

      @foreach($enquiries as $item)
      
       
      
          <a href="#" class="list-group-item">
            <h4 class="list-group-item-heading">{!! $item->title !!}</h4>
            <p class="list-group-item-text">{!! date("F d, Y", strtotime($item->created_at)) !!}</p>
            <p>{!! $item->enquiry !!}</p>
          </a>
      @endforeach

    </div>
@stop
