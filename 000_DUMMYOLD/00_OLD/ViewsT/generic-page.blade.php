@extends('base_page')

@section('title')
    {{ $browser_title }}
@stop
@section('cssBlock') 
    
@stop

@php
    $edit_allowed =   strcmp('#',$page_name);
     //var_dump($page_name);
   //   var_dump($page_id);
   // dd($edit_allowed);
 
@endphp

@section('content')
    
      <div class="row">
          <br>
          <br>

     </div> 
        
        
    <div class="row">
            <div class="col-md-1  ">

            </div>

            <div class="col-md-10">
                
                  @if ((tour\auth\LoggedIn::user()) && (tour\auth\LoggedIn::user()[0]->access_level == 2) && $edit_allowed)
                    <form method="post" action="/admin/page/edit" id="editpage" name="editpage">
                       <article id="editablecontent" class='editablecontent' itemprop="description" style='width: 100%; line-height: 2em;'>
                           {!! $page_content !!}
                       </article>
                       <article class="admin-hidden">
                           <a class="btn btn-primary" href="#!" onclick="saveEditedPage()">Save</a>
                           <a class="btn btn-info" href="#!" onclick="turnOffEditing()">Cancel</a>
                           &nbsp;&nbsp;&nbsp;
                           @if( $page_id==0)
                           <br><br>
                           <input type="text" name="browser_title" placeholder="Enter browser title">
                           @endif
                       </article>
                       <input type="hidden" name="thedata" id="thedata">
                       <input type="hidden" name="old" id="old">
                       <input type="hidden" name="page_id" value="{!! $page_id !!}">
                   </form>
                  
                     @php
                         //dd("Inside   article");
                     @endphp


                  
                  @else
                  {!! $page_content !!}
                     @php
                       //  dd("Inside no article");
                     @endphp
                  
                  @endif


            </div>
            <div class="col-md-1">

            </div>

     </div>
   
@stop

@if($edit_allowed)
        @section('bottomjs')
              @include('admin.admin-js')
        @stop      
@endif


 



 @section('page_make')
  @if( $page_id==0)
     makePageEditable(this);
  @endif   
@stop
