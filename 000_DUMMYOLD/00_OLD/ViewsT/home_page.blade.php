 
 @extends('base_page')
  
 @section('title') 
      Home  
  @stop
  
  
 
  @section('cssBlock') 
     <style>
            .carousel {
                width: 100%;
                margin-top: 50px;
            }
       </style>
  @stop
 
 
   @section('carousel')
 
               <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="5"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="6"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="7"></li>
           
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src= "/assets/aa_slider/0.jpg" alt="image 0">
                        <div class="carousel-caption">
                           0  
                        </div>
                    </div>
                    <div class="item">
                        <img src="/assets/aa_slider/1.jpg" alt="image 1">
                        <div class="carousel-caption">
                            1
                        </div>
                    </div>
                    <div class="item">
                        <img src="/assets/aa_slider/2.jpg" alt="image 2">
                        <div class="carousel-caption">
                          2  
                        </div>
                    </div>
                    
                     <div class="item">
                        <img src="/assets/aa_slider/3.jpg" alt="image 3">
                        <div class="carousel-caption">
                           3
                        </div>
                    </div>
                    
                     <div class="item">
                        <img src="/assets/aa_slider/4.jpg" alt="image 4">
                        <div class="carousel-caption">
                           4
                        </div>
                    </div>
                      <div class="item">
                        <img src="/assets/aa_slider/5.jpg" alt="image 5">
                        <div class="carousel-caption">
                           5
                        </div>
                    </div>
                     
                      <div class="item">
                        <img src="/assets/aa_slider/6.jpg" alt="image 6">
                        <div class="carousel-caption">
                          6
                        </div>
                    </div>
                    <div class="item">
                        <img src="/assets/aa_slider/7.jpg" alt="image 7">
                        <div class="carousel-caption">
                          7
                        </div>
                    </div>
                   
                    
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
  @stop
   
  

 
 @section('content')
 
  <div class="container">
    
     <div class="row">
         <div class="col-md-12">
            <div class="col-md-4 well text-center">
                <h3>Service</h3>
                <span class="glyphicon glyphicon-globe bigger-icon" aria-hidden="true"></span>
                <p>  We offer service which ensures customer satisfaction. Our team will be available any time including day and night on request.  We will make sure proper amenities before the arrival of customer. We have proper tour guides to make sure that you will never miss out important tourist places in Wayanand </p>
                <br> <br>
            </div>

            <div class="col-md-4 well empty-well text-center">
                <h3>Contact</h3>
                <span class="glyphicon glyphicon-earphone bigger-icon" aria-hidden="true"></span>
<p>
<strong>
                   <ul>

                        <li> <a href="#">  Banasura Hill Valley Home Stay </a> </li>
                        <li> <a href="#">  Near Meenmutty Water Falls,Banasura Sagar </a> </li>
                        <li> <a href="#">  Wayanad Kerala,673575 </a> </li>
                        <li> <a href="#">  ph:+91-8129723182, +91-9947177040 </a> </li>
                        <li> <a href="#">  Email:- info@wayanadtoursandtravels.com </a> </li>
                    </ul>
                    </strong>
 <br>
  <br>
 
 
</p>                
            </div>

            <div class="col-md-4 well text-center">
                <h3>Security</h3>
                <span class="glyphicon glyphicon-eye-open bigger-icon" aria-hidden="true"></span>
                <p>Security of our customer is utmost priority to us. We make sure  that apt people will be available at your service on your trip. Proper monitoring of the hotel and travel amenities is done prior to the trip and also during the tour. For any medical urgency our team will guide you to make sure that you get proper service</p>
                <br> <br>
            </div>
             </div>
     </div>
     
     
      <div class="row">
         <div class="col-md-12 col-sm-12">
                <br>
                <br>
         </div>
      </div>
 

 </div>
     
  @stop

