 
 
  
 @extends('base_page')
  
 @section('title') 
      Login Page
  @stop
  
  
 
  @section('cssBlock') 
    
  @stop
 

 @section('content')

  <div class="row">
    <div class="col-md-2  ">

    </div>
      
    <div class="col-md-8">
            <h1>Log in  </h1>
            <hr>
           <form name="loginform" id="loginform" action="/login" method="post" class="form-horizontal">
                        
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">Email</label>
                    <div class="col-md-10">
                      <input type="email" class="form-control" id="email_id" name="email_name" placeholder="user@example.com">
                    </div>
                </div>

                <div class="form-group">
                  <label for="password" class="col-sm-2 control-label">Password</label>
                    <div class="col-md-10">
                      <input type="password" class="form-control" id="password" name='password_name' placeholder="Password">
                    </div>
                </div>
                
               <div class="form-group">
                    <div class="col-md-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-primary">Sign in</button>
                    </div>
                </div>
            </form>


    </div>
    <div class="col-md-2">
        
    </div>

  </div>



  @stop
