 @extends('aa_WorkSpace.ac_blade.aa_base_page') 
 
@section('title') 
      Bootstrap 4.0 Home  Page
 @stop 
 
  @section('cssBlock') 
  <link href="/00_SASS/style.css" rel="stylesheet">  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
   
  @stop

@section('content') 
     "

   <div class="  container-fluid">
       
          
       <div class="row">
           <div class=".col-sm-12 col-md-12">
                  @include('ab_minified_WorkSpace.aa_html_dummy.ab_FAQ.ah_navbar')
           </div>
        
       </div>
       
       <div class="row">
                  
       </div>
    </div> 

   
         

 
      
 
 
           @include('ab_minified_WorkSpace.aa_html_dummy.ab_FAQ.aa_Faq')  
   
             
          

        
           @include('ab_minified_WorkSpace.aa_html_dummy.ab_FAQ.ai_footer_page')   

       
     
 @stop
 
 
 @section('bottomJS') 
 
 
 <script type="text/javascript" src="/01_SCRIPTS/script.js"></script> 
 
 
  
 
 
 <script>
    $(window).scroll(function() {
  $(".slideanim").each(function(){
    var pos = $(this).offset().top;

    var winTop = $(window).scrollTop();
    if (pos < winTop + 600) {
      $(this).addClass("slide");
    }
  });
});         
 
   </script>   
 
     <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   <script>
  $( function() {
    var icons = {
      header: "ui-icon-circle-arrow-e",
      activeHeader: "ui-icon-circle-arrow-s"
    };
    $( "#accordion" ).accordion({
      icons: icons
    });
    $( "#toggle" ).button().on( "click", function() {
      if ( $( "#accordion" ).accordion( "option", "icons" ) ) {
        $( "#accordion" ).accordion( "option", "icons", null );
      } else {
        $( "#accordion" ).accordion( "option", "icons", icons );
      }
    });
  } );
  </script>
   
   
<script>
$(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#topnavid']").on('click', function(event) {

   // Make sure this.hash has a value before overriding default behavior
  if (this.hash !== "") {

    // Prevent default anchor click behavior
    event.preventDefault();

    // Store hash
    var hash = this.hash;

    // Using jQuery's animate() method to add smooth page scroll
    // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
    $('html, body').animate({
      scrollTop: $(hash).offset().top
    }, 900, function(){

      // Add hash (#) to URL when done scrolling (default click behavior)
      window.location.hash = hash;
      });
    } // End if
  });
})
</script>

 
    
    

     
 @stop 
 
 
  