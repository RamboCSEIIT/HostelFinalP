
<!-- If you don't like the style of the default navigation bar, Bootstrap provides an alternative, black navbar: --> 
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
        <div class="navbar-header">
        
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand" href="/">


 
 <strong> Banasura Hill Valley Home Stay </strong>
   
 

                     
                </a>
        </div>



        <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
             
                        <li id ="home-page"><a href="/">Home</a></li>
                        <li id ="about-tour-page"><a href="/about-tour">About</a></li>
                        <li id ="register-page"><a href="/register">Register</a></li>
                        <li id ="about-package-page"><a href="/about-package">Packages</a></li>
                      
                    @if (tour\auth\LoggedIn::user())
                            <li id ="add-entry-page"><a href="/add-enquiry">Enquiry</a></li>
                         
                    @else
                    
                             <li id ="add-entry-page"><a href="/non-reg-enquiry">Enquiry</a></li>
                    @endif
                 </ul>
            
                <ul class="nav navbar-nav navbar-right">
                    
                    
                    @if (tour\auth\LoggedIn::user() && tour\auth\LoggedIn::user()[0]->access_level == 2)
               
                                    <li class="dropdown">
                                        <a id="drop1" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          Admin
                                          <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu" aria-labelledby="drop1">
                                          <li><a class="menu-item" href="#" onclick="makePageEditable(this)">Edit Page</a></li>
                                          <li role="separator" class="divider"></li>
                                          <li><a href="/admin/page/add">Add a page</a></li>
                                           <li role="separator" class="divider"></li>
                                           <li id ="show-entry-page"><a href="/show-enquiry">Show Enquiry</a></li>
                                        </ul>
                                    </li>
                                <li id ="logout-page"><a href="/logout"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span>Logout</a></li>
                                    
                    @elseif(tour\auth\LoggedIn::user())           
                                    <li id ="logout-page"><a href="/logout"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span>Logout</a></li>
                                   
                    @else   
              
                                  <li id ="login-page"><a href="/login">Login</a></li>
                    @endif
                 </ul>            
            
        </div>

   
  </div>
</nav>



  

