@section('image_pkg') 

 



@foreach ($galleryPackage as  $index => $gallery_item)



   @if($index%3==0)
           <div class="card-deck" style="margin-top: 20px">

    @endif


    @php
    $id = "package_".$index;
    @endphp



    <div class="card">


        <div class="card-body  text-white" id = "{!!$id !!}">




            <img src="{!!  $gallery_item->image_link !!}" alt="Smiley face" class="img-fluid"  >
          

            @if( $gallery_item->tentative==1)
             <h2 class="card-title mt-2 text-center" style="background: greenyellow;  border-radius: 40px; padding: 1px;font-size: 30px">{!!  $gallery_item->heading !!}</h2>
  
            <h5 class="card-subtitle text-center"><span>&#8377;</span>  Cost Tentative            </h5>

            @else
                      <h2 class="card-title mt-2 text-center" style="background: greenyellow;  border-radius: 40px; padding: 1px;">{!!  $gallery_item->heading !!}</h2>
  
              <h3 class="card-title mt-2 text-center">{!!  $gallery_item->subheading !!}</h3>
            <h5 class="card-subtitle text-center"><span>&#8377;</span> {!!  $gallery_item->cost !!} per couple
            </h5>

            @endif
            
                           @if(strlen( $gallery_item->day1 )!=0)

                <div class="card-text mt-3 ">

                    {!!  $gallery_item->day1 !!}  
                </div> 
                @endif

                @if(strlen( $gallery_item->day2 )!=0)

                <div class="card-text mt-3  ">

                    {!!  $gallery_item->day2 !!}  
                </div> 
                @endif


                @if(strlen( $gallery_item->day3 )!=0)

                <div class="card-text mt-3  ">

                    {!!  $gallery_item->day3 !!}  
                </div> 
                @endif

                @if(strlen( $gallery_item->day4 )!=0)

                <div class="card-text mt-3  ">

                    {!!  $gallery_item->day4 !!}  
                </div> 
                @endif


                @if(strlen( $gallery_item->day5 )!=0)

                <div class="card-text mt-3  " >

                    {!!  $gallery_item->day5 !!}  
                </div> 
                @endif

   

 


        </div>
    </div>


    @if($index%3==2 || sizeof($galleryPackage)-1==$index)
    </div>
   
  
    @endif

@endforeach


@stop 

<div id="package" >
    <h2 class= "text-center"> <span class="font_package_heading"> Packages</span></h2><br>
    <br> 
    <div class="container">
       



            @yield('image_pkg')  


      




    </div>

    <br> 
    <br> 
    <br> 





</div>

